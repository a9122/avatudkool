<?php

namespace App\Models;

use App\Models\Traits\HasUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function __;
use function current_user;
use function explode;
use function is_numeric;
use function trim;

/**
 * App\Models\Grade
 *
 * @property int $id
 * @property int $level
 * @property string $group
 * @property string|null $description
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @property-read int|null $students_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Grade newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Grade newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Grade query()
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Grade whereUserId($value)
 * @mixin \Eloquent
 */
class Grade extends Model {
	const LEVEL_MAX = 9;
	use HasFactory, HasUser;
	static $importErr = null;

	protected $guarded = ['id'];
	protected $table   = 'grades';

	public static function findOrCreate($name) {
		if (empty(trim($name))) {
			self::$importErr = __('app.import.errors.grade.name');
			return null;
		}

		[$level, $group] = explode(' ', trim($name), 2);
		if (!is_numeric($level)) {
			self::$importErr = __('app.import.errors.grade.level');

			return null;
		}
		if (empty($group)) {
			self::$importErr = __('app.import.errors.grade.group');

			return null;
		}

		$res = self::where('level', $level)
			->where('group', $group)
			->first();

		if ($res) {
			return $res;
		}

		$res = static::create([
			'level'   => $level,
			'group'   => $group,
			'user_id' => current_user()->id,
		]);

		return $res;
	}

	public static function getGradeByOffset($grade_id, $offset_years = -1) {
		$current = self::find($grade_id);

		return self::where('level', $current->level + (int)($offset_years))->where('group', $current->group)
			->first();
	}

	public static function getPrevGrade($grade_id) {
		return self::getGradeByOffset($grade_id, -1);
	}

	public static function getNextGrade($grade_id) {
		return self::getGradeByOffset($grade_id, +1);
	}

	public function getNextGradeAttribute() {
		return self::getNextGrade($this->id);
	}

	public function getPrevGradeAttribute() {
		return self::getPrevGrade($this->id);
	}

	public function getNameAttribute() {
		return trim($this->level.' "'.$this->group.'"');
	}

	public function students() {
		return $this->hasMany(Student::class)->orderBy('first_name');
	}

	public function getStudents($promotion_key = null) {
		if (!empty($promotion_key)) {
			return $this->students()->where('grade_promotion', $promotion_key)->orderBy('first_name');
		}

		return $this->students()->whereNull('grade_promotion')->orderBy('first_name');
	}

	public function exams() {
		return Exam::where('grade_level', $this->level)->get();
	}
}