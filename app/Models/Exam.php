<?php

namespace App\Models;

use App\Casts\Json;
use App\Models\Traits\HasUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function array_filter;
use function current_user;
use function in_array;
use function is_array;

/**
 * App\Models\Exam
 *
 * @property int                                                               $id
 * @property string|null                                                       $name
 * @property string|null                                                       $description
 * @property int                                                               $subject_id
 * @property int|null                                                          $grade_level
 * @property string                                                            $type
 * @property int|null                                                          $user_id
 * @property array|null                                                        $_data
 * @property \Illuminate\Support\Carbon|null                                   $created_at
 * @property \Illuminate\Support\Carbon|null                                   $updated_at
 * @property string                                                            $semester
 * @property-read \App\Models\Subject                                          $subject
 * @property-read \App\Models\User|null                                        $user
 * @method static \Illuminate\Database\Eloquent\Builder|Exam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exam query()
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereGradeLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereSemester($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereUserId($value)
 * @mixin \Eloquent
 * @property-read mixed                                                        $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Score[] $scores
 * @property-read int|null                                                     $scores_count
 */
class Exam extends Model {
	use HasFactory, HasUser;

	const SEMESTER_AUTUMN = 'autumn';
	const SEMESTER_SPRING = 'spring';
	const TYPE_GOVERNMENT = 'government';
	const TYPE_SCHOOL     = 'school';
	protected $casts   = [
		'_data' => Json::class,
	];
	protected $dates   = ['created_at'];
	protected $guarded = ['id'];

	public static function findByFilter($filter = [
		'grade_id'    => null,
		'grade_level' => null,
		'subject_ids' => null,
		'type'        => null,
		'semester'    => null,
	]) {
		$f = array_filter($filter);
		$q = self::query();
		foreach($f ?? [] as $key => $value) {
			switch ($key) {
				case 'grade_id':
					if (isset($value)) {
						$grade = Grade::find($value);
						if ($grade) {
							$q->where('grade_level', $grade->level);
						}
					}
					break;
				case 'subject_ids':
					if (!empty($value)) {
						$q->whereIn('subject_id', is_array($value) ? $value : [$value]);
					}
					break;
				case 'type':
					if (in_array($value, [Exam::TYPE_SCHOOL, Exam::TYPE_GOVERNMENT], true)) {
						$q->where('type', $value);
					}
					break;
				case 'semester':
					if (in_array($value, [Exam::SEMESTER_AUTUMN, Exam::SEMESTER_SPRING], true)) {
						$q->where('semester', $value);
					}
					break;
				default:
					$q->where($key, $value);
			}
		}

		return $q->get();
	}

	public static function getAllByGrade($grade_id) {
		return self::findByFilter(['grade_id' => $grade_id]);
	}

	/**
	 * The "booted" method of the model.
	 *
	 * @return void
	 */
	protected static function booted() {
		static::creating(function($record) {
			if (empty($record->name)) {
				$subj         = Subject::find($record->subject_id);
				$record->name = "{$subj->name}, {$record->grade_level}, $record->type, $record->semester";
			}
			$record->user_id = current_user()->id;
		});
	}

	public function getTitleAttribute() {
		return trim($this->name ?: "{$this->subject->name}, {$this->grade_level}, $this->type, $this->semester");
	}

	public function scores() {
		return $this->hasMany(Score::class)->orderBy('exam_id');
	}

	public function subject() {
		return $this->belongsTo(Subject::class);
	}

}
