<?php

namespace App\Models;

use App\Casts\Json;
use App\Models\Traits\HasUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function current_user;
use function date;
use function strtotime;

/**
 * App\Models\Score
 *
 * @property int $id
 * @property int $student_id
 * @property int $exam_id
 * @property int $grade_id
 * @property \Illuminate\Support\Carbon $date
 * @property int $score
 * @property int|null $user_id
 * @property array|null $_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Exam $exam
 * @property-read mixed $score_color
 * @property-read mixed $semester
 * @property-read \App\Models\Grade $grade
 * @property-read \App\Models\Student $student
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Score newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Score newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Score query()
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereUserId($value)
 * @mixin \Eloquent
 * @property-read mixed $color
 */
class Score extends Model {
	use HasFactory, HasUser;

	const COLORS     = [self::COLOR_GREEN, self::COLOR_YELLOW, self::COLOR_RED];
	const SEMESTER_1 = 'autumn';
	const SEMESTER_2 = 'spring';

	const COLOR_RED    = '#E53935';
	const COLOR_YELLOW = '#FCCE14';//'#EB9100';
	const COLOR_GREEN  = '#43A047';

	protected $guarded = ['id', 'updated_at'];
	protected $dates   = ['date', 'created_at'];
	protected $casts   = [
		'_data' => Json::class,
		'date'  => 'datetime:d.m.Y',
	];

	/**
	 * @param \App\Models\Exam    $exam
	 * @param \App\Models\Student $student
	 * @param \App\Models\Grade   $grade
	 * @param int|string          $score
	 * @param string              $date
	 *
	 * @return \App\Models\Score|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
	 */
	public static function findOrCreate($exam, $student, $grade, $score, $date) {
		if (!isset($exam, $student, $grade)) {
			return null;
		}

		$res = self::where('exam_id', $exam->id)
			->where('student_id', $student->id)
			->where('grade_id', $grade->id)
			->first();


		if ($res) {
			$res->update(['score' => (int)$score]);

			return $res;
		}

		$res = static::create([
			'student_id' => $student->id,
			'grade_id'   => $grade->id,
			'exam_id'    => $exam->id,
			'date'       => Carbon::parse($date)->format('y-m-d'),
			'score'      => (int)$score,
			'user_id'    => current_user()->id,
		]);

		if (!$res) {
			/* error log.. */
		}

		return $res;
	}

	public function getSemesterAttribute() {
		return ((int)date('m', strtotime($this->attributes['date'])) > 6) ? self::SEMESTER_1 : self::SEMESTER_2;
	}

	public function getScoreAttribute() {
		return (int)$this->attributes['score'];
	}

	public function getColorAttribute() {
		if ($this->attributes['score'] <= 50) {
			return self::COLOR_RED;
		}
		if ($this->attributes['score'] <= 75) {
			return self::COLOR_YELLOW;
		}
		if ($this->attributes['score'] <= 100){
			return self::COLOR_GREEN;
		}

		return '#333';
	}

	public function student() {
		return $this->belongsTo(Student::class);
	}

	public function exam() {
		return $this->belongsTo(Exam::class);
	}

	public function grade() {
		return $this->belongsTo(Grade::class);
	}
}
