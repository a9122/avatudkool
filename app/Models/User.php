<?php

namespace App\Models;

use App\Casts\Json;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use function get_gravatar;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property \Illuminate\Support\Carbon|null $last_login
 * @property array|null $_data
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Grade[] $grades
 * @property-read int|null $grades_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Score[] $scores
 * @property-read int|null $scores_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @property-read int|null $subjects_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail {
	use HasApiTokens, HasFactory, Notifiable;

	const TYPE_ADMIN   = 'admin';
	const TYPE_TEACHER = 'teacher';
	/**
	 * The attributes that should be cast.
	 *
	 * @var array<string, string>
	 */
	protected $casts = [
		'email_verified_at' => 'datetime:d.m.Y H:i:s',
		'last_login'        => 'datetime:d.m.Y H:i:s',
		'_data'             => Json::class,
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	//protected $guarded = [];
	/**
	 * The attributes that should be hidden for serialization.
	 *
	 * @var array<int, string>
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];
	protected $fillable = [
		'name',
		'email',
		'type',
		'password'
	];

	protected static function booted() {
		static::created(function(self $record) {
		});

		static::updated(function($record) {
		});

		static::deleted(function($record) {
		});
	}

	public function adminlte_desc() {
		return $this->description;
	}

	public function adminlte_image() {
		return get_gravatar($this->email);
	}

	public function adminlte_profile_url() {
		return 'accounts/'.$this->id;
	}

	public function is_admin() {
		return $this->type == self::TYPE_ADMIN;
	}

	public function subjects() {
		return $this->hasMany(Subject::class)->orderBy('name');
	}

	public function grades() {
		return $this->hasMany(Grade::class)->orderBy('level');
	}

	public function students() {
		return $this->hasMany(Student::class)->orderBy('first_name');
	}

	public function scores() {
		return $this->hasMany(Score::class)->orderBy('date', 'DESC');
	}
}
