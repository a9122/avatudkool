<?php

namespace App\Models\Traits;

use App\Models\User;
use function current_user;

/**
 * @method static creating(\Closure $param)
 * @method belongsTo(string $class)
 */
trait HasUser {
	/**
	 * The "booted" method of the model.
	 *
	 * @return void
	 */
	protected static function booted() {
		static::creating(function($record) {
			$record->user_id = current_user()->id;
		});
	}

	public function user() {
		return $this->belongsTo(User::class);
	}
}

