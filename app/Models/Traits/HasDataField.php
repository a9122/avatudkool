<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;
use Traversable;
use function current_user;
use function is_iterable;

trait HasDataField {
	/**
	 * The "booted" method of the model.
	 *
	 * @return void
	 */
	public static function booted() {
		Model::retrieved(function($record) {
			$record->addDataFieldAttributes();
		});
	}
	/**
	 * Add attributes, even though they are stored as JSON.
	 *
	 */
	public function addDataFieldAttributes() {
		$columns = ['_data'];
		foreach($columns as $key => $column) {
			if (!isset($this->attributes[$column])) {
				continue;
			}

			$column_contents = $this->{$column};

			if ($this->shouldDecodeFake($column)) {
				$column_contents = json_decode($column_contents, true);
			}

			if (is_iterable($column_contents)) {
				foreach($column_contents as $data_field_name => $data_field_value) {
					/** @var Model $this */
					$this->setAttribute($data_field_name, $data_field_value);
				}
			}
		}
	}

	/**
	 * Determine if this fake column should be json_decoded.
	 *
	 * @param $column string fake column name
	 *
	 * @return bool
	 */
	public function shouldDecodeFake($column) {
		return !array_key_exists($column, $this->casts);
	}

	/**
	 * Determine if this fake column should get json_encoded or not.
	 *
	 * @param $column string fake column name
	 *
	 * @return bool
	 */
	public function shouldEncodeFake($column) {
		return !array_key_exists($column, $this->casts);
	}
}

