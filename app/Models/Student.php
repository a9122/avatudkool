<?php

namespace App\Models;

use App\Casts\Json;
use App\Models\Traits\HasUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use RKD\PersonalIdCode\PersonalIdCode;
use function current_user;
use function now;
use function time;

/**
 * App\Models\Student
 *
 * @property int         $id
 * @property string      $first_name
 * @property string      $last_name
 * @property string|null $birth_year
 * @property int         $grade_id
 * @property string|null $isikukood
 * @property string                                                            $lang
 * @property int|null                                                          $user_id
 * @property mixed|null                                                        $_data
 * @property \Illuminate\Support\Carbon|null                                   $created_at
 * @property \Illuminate\Support\Carbon|null                                   $updated_at
 * @property string|null                                                       $deleted_at
 * @property-read \App\Models\Grade                                            $grade
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Score[] $scores
 * @property-read int|null                                                     $scores_count
 * @property-read \App\Models\User|null                                        $user
 * @method static \Illuminate\Database\Eloquent\Builder|Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereBirthYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereIsikukood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereUserId($value)
 * @mixin \Eloquent
 * @property int                                                               $status 0 - not active; 1 - active; 2 -
 *           alumnus;
 * @property string|null                                                       $description
 * @property-read mixed                                                        $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Student whereStatus($value)
 */
class Student extends Model {
	use HasFactory, HasUser;

	const LANG_LABELS = [
		'est' => 'Estonian',
		'rus' => 'Russian',
	];

	protected $casts   = [
		'updated_at' => 'datetime:d.m.Y H:i:s',
		'_data'      => Json::class,
	];
	protected $dates   = ['updated_at', 'created_at'];
	protected $guarded = ['id'];
	protected $table   = 'students';
	/**
	 * @var \App\Models\PersonalIdCode
	 */
	private $personalCode;

	public static function getOrCreate($full_name, $isikukood, $lang = 'est', $grade_id = null, $status = 1) {
		if (empty($full_name) || empty($isikukood)) {
			return null;
		}

		[$first_name, $last_name] = explode(' ', $full_name, 2);

		$student = self::where('isikukood', $isikukood)->first();

		if ($student) {
			$student->update([
				'first_name' => $first_name,
				'last_name'  => $last_name,
			]);

			return $student;
		}

		$student = static::firstOrCreate([
			'first_name' => $first_name,
			'last_name'  => $last_name,
			'isikukood'  => $isikukood
		], [
			'grade_id'   => $grade_id ?? Grade::firstWhere('id', '>', 0)->id,
			'lang'       => $lang ?? 'est',
			'status'     => $status,
			'user_id'    => current_user()->id,
			'created_at' => now(),
		]);

		return $student;
	}

	protected static function booted() {
		static::creating(function($record) {
			$record->user_id = current_user()->id;
		});

		static::updating(function(Model $record) {
			if ($record->isDirty('grade_id')) {
				$data                      = $record->_data ?? ['transfers' => []];
				$data['transfers'][time()] = [
					'from' => $record->getOriginal('grade_id', '-'),
					'to'   => $record->grade_id,
				];

				$record->_data = $data;
			}
		});
	}

	public static function validateIsikukood($code) {
		return (new PersonalIdCode($code))->validate();
	}

	public function exams() {
		$grade_level = $this->grade->level;

		return Exam::where('grade_level', $grade_level)->get();
	}

	public function getAgeAttribute() {
		return $this->getPersonalCode()->getAge();
	}

	public function getLanguageAttribute() {
		return self::LANG_LABELS[$this->lang] ?? $this->lang;
	}

	public function getCurrGradeAttribute() {
		return $this->grade->name;
	}

	public function getBirthDateAttribute() {
		return $this->getPersonalCode()->getBirthDate()->format('d.m.Y');
	}

	public function getFullNameAttribute() {
		return trim($this->first_name.' '.$this->last_name);
	}

	/**
	 * Get personalCode
	 *
	 * @return \RKD\PersonalIdCode\PersonalIdCode
	 */
	public function getPersonalCode():PersonalIdCode {
		if (!$this->personalCode) {
			$this->personalCode = new PersonalIdCode($this->isikukood);
		}

		return $this->personalCode;
	}

	/**
	 * Scope a query to only include active students.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 *
	 * @return void
	 */
	public function scopeActive($query) {
		$query->where('status', 1);
	}

	/**
	 * Scope a query to only include active users.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 *
	 * @return void
	 */
	public function scopeGraduated($query) {
		$query->where('status', 2)->orWhereNotNull('graduated_at');
	}

	public function grade() {
		return $this->belongsTo(Grade::class);
	}

	public function image() {
		return '/img/default-student.png';
	}

	public function scores() {
		return $this->hasMany(Score::class)->orderBy('subject_id');
	}
}
