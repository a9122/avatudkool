<?php

use App\Models\User;
use Illuminate\Support\Facades\Log as LogAlias;

if (!function_exists('is_admin_panel')) {
	function is_admin_panel() {
		return (request()->segment(1) === config('backpack.base.route_prefix', 'admin'));
	}
}

if (!function_exists('str_limit')) {
	/**
	 * Limit the number of characters in a string.
	 *
	 * @param string $value
	 * @param int    $limit
	 * @param string $end
	 *
	 * @return string
	 */
	function str_limit($value, $limit = 100, $end = '...') {
		return Str::limit($value, $limit, $end);
	}
}

if (!function_exists('asset_version')) {
	/**
	 * Return asset file version
	 *
	 * @param string $path File uri
	 *
	 * @return string version string(e.g.: ?v=12345678)
	 */
	function asset_version($path) {
		$file_path = public_path().DIRECTORY_SEPARATOR.$path;

		return (file_exists($file_path) ? '?v='.filemtime($file_path) : '');
	}
}

if (!function_exists('public_storage_path')) {
	/**
	 * Generate an asset path for the application.
	 *
	 * @param string $path
	 * @param bool   $secure
	 *
	 * @return string
	 */
	function public_storage_path($path) {
		return storage_path('public/'.$path);
	}
}
if (!function_exists('storage_url')) {
	/**
	 * Generate an asset path for the application.
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	function storage_url($path) {
		return Storage::url($path);
	}
}
if (!function_exists('asset_ext')) {
	/**
	 * Generate an asset path with version param
	 *
	 * @param string $path
	 * @param bool   $secure
	 *
	 * @return string
	 */
	function asset_ext($path, $secure = null) {
		return app('url')->asset($path, $secure).asset_version($path);
	}
}

if (!function_exists('text')) {
	/**
	 * Clean text.
	 *
	 * @param string $str             input text
	 * @param bool   $use_html_entity use html entities
	 *
	 * @return string
	 */
	function text($str, $use_html_entity = false):string {
		$text = trim(strip_tags($str));

		return $use_html_entity ? htmlentities($text) : $text;
	}
}

if (!function_exists('var_export_ext')) {
	/**
	 * Export var to PHP
	 *
	 * @return string
	 */
	function var_export_ext($var, $indent = '') {
		switch (gettype($var)) {
			case 'string':
				$result = '"'.addcslashes($var, "\\\$\"\r\n\t\v\f").'"';
				break;
			case 'array':
				$indexed = array_keys($var) === range(0, count($var) - 1);
				$r       = [];
				foreach($var as $key => $value) {
					$r[] = "{$indent}\t"
						.($indexed ? '' : var_export_ext($key).' => ')
						.var_export_ext($value, "{$indent}\t");
				}

				$result = "[\n".implode(",\n", $r)."\n".$indent.']';
				break;
			case 'boolean':
				$result = $var ? 'TRUE' : 'FALSE';
				break;
			default:
				$result = var_export($var, true);
		}

		return $result;
	}
}

if (!function_exists('goback_url')) {
	/**
	 * Store go-back url using session
	 *
	 * @return string|null
	 */
	function goback_url($goback_url = null) {
		if ($goback_url === null) {
			return session()->get('go-back.url', $goback_url);
		}

		if (trim($goback_url) === false) {
			session()->forget('go-back.url');

			return null;
		}

		session()->put('go-back.url', $goback_url ?: url()->full());
	}
}


if (!function_exists('number_only')) {
	function number_only($str) {
		if (!empty($str)) {
			return (int)filter_var($str, FILTER_SANITIZE_NUMBER_INT);
		}

		return null;
	}
}

if (!function_exists('static_page_url')) {
	function static_page_url($page_id = null) {
		$res = StaticPage::find($page_id, ['slug']);

		return $res->slug ?? null;
	}
}

if (!function_exists('url_get_contents')) {
	function url_get_contents($url, $timeout_sec = 4) {
		$curlSession = curl_init();
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($curlSession, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($curlSession, CURLOPT_TIMEOUT, 4); //timeout in seconds

		$data       = curl_exec($curlSession);
		$curl_errno = curl_errno($curlSession);
		$curl_error = curl_error($curlSession);
		curl_close($curlSession);
		if ($curl_errno > 0) {
			$res = false;
			LogAlias::error("cURL Error ($curl_errno): $curl_error\n");
		} else {
			$res = json_decode($data, 1);
		}

		return $res;
	}
}

if (!function_exists('get_gravatar')) {
	/**
	 * Get either a Gravatar URL or complete image tag for a specified email address.
	 *
	 * @param string  $email    The email address
	 * @param string  $s        Size in pixels, defaults to 80px [ 1 - 2048 ]
	 * @param string  $d        Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
	 * @param string  $r        Maximum rating (inclusive) [ g | pg | r | x ]
	 * @param boolean $img_html True to return a complete IMG tag False for just the URL
	 * @param array   $atts     Optional, additional key/value attributes to include in the IMG tag
	 *
	 * @return String containing either just a URL or a complete image tag
	 * @source https://gravatar.com/site/implement/images/php/
	 */
	function get_gravatar($email, $s = 40, $d = 'mp', $r = 'g', $img_html = false, $atts = []) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5(strtolower(trim($email)));
		$url .= "?s=$s&d=$d&r=$r";
		if ($img_html) {
			$url = '<img src="'.$url.'"';
			foreach($atts as $key => $val) {
				$url .= ' '.$key.'="'.$val.'"';
			}
			$url .= ' />';
		}

		return $url;
	}
}
if (!function_exists('records_count')) {
	function records_count($table) {
		$is_admin = current_user()->is_admin();
		$user_id  = current_user()->id;
		if ($table === 'users') {
			return DB::table($table)->where('id', '!=', current_user()->id)->count();
		}

		if (strpos($table, '.') !== false) {
			$q = explode('.', $table);

			if (strpos($q[1], ':') !== false) {
				$qv = explode(':', $table);

				return DB::table($q[0])->where($qv[0], $qv[1])->get()->count();
			}

			return DB::table($q[0])->select($q[1] ?? 'id')->distinct()->get()->count();
		}

		if (strpos($table, '!') !== false) {
			$q = explode('!', $table);

			return DB::table($q[0])->whereNull($q[1])->count();
		}

		return $is_admin
			? DB::table($table)->count()
			: DB::table($table)->where('user_id', '=', $user_id)->count();
	}
}

if (!function_exists('html_attributes')) {
	function html_attributes(array $attributes, $excludes = []) {
		$res = [];
		foreach($attributes as $k => $v) {
			if (!empty($excludes) && in_array($k, $excludes)) {
				continue;
			}

			if ($k === 'data') {
				foreach($v ?? [] as $dk => $dv) {
					$res[] = trim($k).'-'.$dk.(
						is_bool($dv)
							? ''
							: '="'.htmlspecialchars($dv, ENT_QUOTES | ENT_SUBSTITUTE).'"'
						);
				}
				continue;
			}
			if (is_bool($v)) {
				if ($v) {
					$res[] = trim($k);
				}
				continue;
			}

			$res[] = trim($k).(is_bool($v)
					? ''
					: '="'.htmlspecialchars($v, ENT_QUOTES | ENT_SUBSTITUTE).'"');
		}

		return trim(implode(' ', $res));
	}
}

if (!function_exists('mb_ucfirst')) {
	function mb_ucfirst($text) {
		if (!is_string($text)) {
			return $text;
		}

		return mb_strtoupper(mb_substr($text, 0, 1)).mb_strtolower(mb_substr($text, 1));
	}
}

if (!function_exists('mb_lcfirst')) {
	function mb_lcfirst($text) {
		return mb_strtolower(mb_substr($text, 0, 1)).mb_substr($text, 1);
	}
}

function str_to_utf8(string $_str):string {
	$encoding = mb_detect_encoding($_str, mb_detect_order(), false);
	$str      = ($encoding === 'UTF-8') ? mb_convert_encoding($_str, 'UTF-8', 'UTF-8') : $_str;

	return iconv(mb_detect_encoding($str, mb_detect_order(), false), 'UTF-8//IGNORE', $_str);
}

function adv_trim($_val) {
	return trim($_val, " ,\t\n\r\0\x0B");
}

function file_read_csv($_filename, $_filter = null, $_row_callback = null, array $_options = []) {
	if (!file_exists($_filename)) {
		throw new RuntimeException("File `{$_filename}` not found!");
	}

	$f__detect_delimiter = function($fpath) {
		$delimiters = [',' => 0, ';' => 0, "\t" => 0, '|' => 0,];
		$firstLine  = '';
		$handle     = fopen($fpath, 'rb');
		if ($handle) {
			$firstLine = fgets($handle);
			fclose($handle);
		}
		if ($firstLine) {
			foreach($delimiters as $delimiter => &$count) {
				$count = count(str_getcsv($firstLine, $delimiter));
			}

			return array_search(max($delimiters), $delimiters, true);
		}

		return key($delimiters);
	};

	$delimiter   = $_options['delimiter'] ?? $f__detect_delimiter($_filename);
	$skip_rows   = $_options['skip_rows'] ?? [];
	$offset      = $_options['offset'] ?? -1;
	$limits      = $_options['offset'] ?? 0;
	$id_col_name = $_options['key_name'] ?? null;
	$lowercase_key = $_options['key_lowercase'] ?? true;


	/* get cell data into $result, and return is value defined */
	$f__get_next_check = static function($_file, &$_values) use ($delimiter) {
		$_values = fgetcsv($_file, 0, $delimiter);

		return !empty($_values);
	};

	/* get value (check need to rename) */
	$f__get_value = static function($_val, $_key = '', &$_dataset = null, $_curr_row = []) use ($_filter) {
		$value = str_to_utf8(trim($_val));
		/* callable filter */
		if (!is_null($_key) && is_callable($_filter)) {
			return $_filter($_val, $_key, $_dataset, $_curr_row);
		}

		return adv_trim($value);
	};

	$result = [];

	$fp = @fopen($_filename, 'rb');
	if ($fp) {
		$col_names = [];
		$row_data  = [];
		$row_index = 0;
		/* read data row-by-row */
		try {
			while ($f__get_next_check($fp, $row_data)) {
				$row_index++;
				if (($limits > 0) && $row_index === $limits) {
					break;
				}

				if ($row_index === 1) { /* read "head" with column names */
					foreach($row_data as $col_name) {
						$col_names[] = $f__get_value($col_name, null);
					}

					continue;
				}
				/* skip data by conditions */
				if (($row_index < $offset) || in_array($row_index, $skip_rows)) {
					continue;
				}
				$curr_row = ['row' => $row_index,];

				/* read data row cells */
				$halt = false;
				foreach($row_data as $key => $value) {

					$col_name  = $col_names[$key] ?: 'col'.$value;
					$col_value = $f__get_value($value, $col_name, $curr_row, $row_data);

					if ($lowercase_key) {
						$col_name = mb_strtolower($col_name);
					}
					/* store data only for needed fields */
					if ($col_value !== false) {
						$curr_row[$col_name] = $col_value;
					}

					if ($col_value === null) {
						$halt = true;
						break;
					}
				}

				if ($halt) {
					continue;
				}

				$key = isset($id_col_name) ? ($curr_row[$id_col_name] ?? $row_index) : $row_index;
				/* push to result */
				$curr_row = array_filter($curr_row, function($v) { return !empty($v); });
				if (is_callable($_row_callback)) {
					$_row_callback($curr_row, $key);
				}

				$result[$key] = $curr_row;
				unset($row_data, $curr_row);
			}
		} finally {
			fclose($fp);
			unset($fp, $col_names);
		}
	}

	return $result;
}

function check_url($url) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch , CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    $headers = curl_getinfo($ch);
    curl_close($ch);

    return in_array((int)$headers['http_code'], [200,301,302]);
}

if (!function_exists('current_user')) {
	/**
	 * Get current user
	 *
	 * @return \App\Models\User | object
	 */
	function current_user() {
		if (Auth::guest() || Auth::user() === null) {
			return json_decode(json_encode(['id' => 0]));
		}

		return Auth::user();
	}
}
if (!function_exists('all_users')) {
	/**
	 * Get current user
	 *
	 * @return \App\Models\User | object
	 */
	function all_users() {
		return User::get(['id', 'name'])->where('id', '!=', Auth::user()->id)->all();
	}
}

if (!function_exists('array_only')) {
	/**
	 * Get a subset of the items from the given array.
	 *
	 * @param array        $array
	 * @param array|string $keys
	 *
	 * @return array
	 */
	function array_only($array, $keys) {
		return Arr::only($array, $keys);
	}
}

if (!function_exists('array_pluck')) {
	/**
	 * Pluck an array of values from an array.
	 *
	 * @param array             $array
	 * @param string|array      $value
	 * @param string|array|null $key
	 *
	 * @return array
	 */
	function array_pluck($array, $value, $key = null) {
		return Arr::pluck($array, $value, $key);
	}
}

if (!function_exists('paypal_ipn_verify')) {
	function paypal_ipn_verify() {
		$paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
		if (env('ENABLE_PAYPAL_SANDBOX', 0) == 1) {
			$paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		}

		// STEP 1: read POST data
		// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
		// Instead, read raw POST data from the input stream.
		$raw_post_data  = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost         = [];
		foreach($raw_post_array as $keyval) {
			$keyval = explode('=', $keyval);
			if (count($keyval) == 2) {
				$myPost[$keyval[0]] = urldecode($keyval[1]);
			}
		}
		// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
		$req = 'cmd=_notify-validate';
		if (function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach($myPost as $key => $value) {
			if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}

		// STEP 2: POST IPN data back to PayPal to validate
		$ch = curl_init($paypal_action_url);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Connection: Close']);

		if (!($res = curl_exec($ch))) {
			// error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);

		// STEP 3: Inspect IPN validation result and act accordingly
		if (strcmp($res, "VERIFIED") == 0) {
			return true;
		} elseif (strcmp($res, "INVALID") == 0) {
			return false;
		}

		return false;
	}
}

function arr_to_attr_val($arr) {
	return htmlspecialchars(json_encode($arr), ENT_QUOTES);
}