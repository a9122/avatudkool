<?php

namespace App\Services;

use App\Models\Grade;
use JeroenNoten\LaravelAdminLte\Menu\Builder;
use function current_user;
use function route;

class MenuBuilder {
	public static function sidebar(Builder $menu) {
		$min_grade = Grade::min('level');
		$max_grade = Grade::max('level');

		$classrooms_items = [];

		for($lvl = $min_grade; $lvl <= $max_grade; $lvl++) {
			$grades  = Grade::where('level', $lvl)->withCount('students')->get();
			$submenu = [];
			$count   = 0;
			foreach($grades as $grade) {
				$count     += $grade->students_count;
				$submenu[] = [
					'text'       => $grade->name,
					'label'      => $grade->students_count,
					'url'        => route('school.classroom.scores', ['grade' => $grade->id]),
					'icon'       => 'fas mr-1 fa-chalkboard',
					'icon_color' => 'silver',
				];
			}
			$classrooms_items[] = [
				'text'    => 'Grade '.$lvl,
				'label'   => $count,
				'icon'    => 'fas fa-chalkboard-teacher',
				'submenu' => $submenu,
			];
		}

		$menu->addAfter('home', [
			'text'       => 'Classrooms',
			'icon_color' => 'info',
			'icon'       => 'fas fa-chalkboard-teacher mr-1',
			'submenu'    => $classrooms_items,
		], [
			'text'        => 'Teachers',
			'url'         => '/teachers',
			'active'      => ['/teachers', 'regex:@^teacher(.*)$@'],
			'icon_color'  => 'info',
			'icon'        => 'fas fa-user-friends mr-1',
			'label'       => 'users.type:teacher',
			'label_color' => 'info',
		]);

		if (current_user()->is_admin()) {
			$menu->add('Dashboard');
			$menu->add(
				[
					'text'        => 'Accounts',
					'url'         => '/accounts',
					'active'      => ['/accounts', 'regex:@^account(.*)$@'],
					'icon_color'  => '',
					'icon'        => 'fas fa-user-friends mr-1',
					'label'       => 'users',
					'label_color' => 'warning',
				],
				[
					'text'        => 'Subjects',
					'url'         => '/subjects',
					'active'      => ['regex:@^subject(.*)$@'],
					'icon_color'  => '',
					'icon'        => 'fas fa-book-open mr-1',
					'label'       => 'subjects',
					'label_color' => 'warning',
				],
				[
					'text'        => 'Grades',
					'url'         => '/grades',
					// 'custom_class' => 'disabled',
					'active'      => ['regex:@^grade(.*)$@'],
					'icon_color'  => '',
					'icon'        => 'fas fa-tags mr-1',
					'label'       => 'grades',
					'label_color' => 'warning',
				],
				[
					'text'        => 'Exams',
					'url'         => '/exams',
					'active'      => ['regex:@^exam(.*)$@'],
					'icon_color'  => '',
					'icon'        => 'fas fa-boxes mr-1',
					'label'       => 'exams',
					'label_color' => 'warning',
				],
				[
					'text'        => 'Students',
					'url'         => '/students',
					'active'      => ['regex:@^student(.*)$@'],
					'icon_color'  => '',
					'icon'        => 'fas fa-users mr-1',
					'label'       => 'students',
					'label_color' => 'warning',
				],
				[
					'text'        => 'Scores',
					'url'         => '/scores',
					'active'      => ['regex:@^score(.*)$@'],
					'icon_color'  => '',
					'icon'        => 'fas fa-award mr-1',
					'label'       => 'scores',
					'label_color' => 'warning',
				],

			);
		}
		$menu->add(
			[
				'type'         => 'fullscreen-widget',
				'topnav_right' => true,
			],
		);
	}
}


