<?php

namespace App\Services;

use App\Models\Exam;
use App\Models\Grade;
use App\Models\Score;
use App\Models\Student;
use function array_filter;

class ScoreFinder {
	public static function gradeScores($grade_id, $filter, $terms = 0) {
		// Student::where('grade_id', $grade_id)->get();
		$curr_grade = Grade::find($grade_id);
		if (!$curr_grade) {
			return [];
		}
		$students = $curr_grade->students;
		if (!$students) {
			return [];
		}

		$terms  = $terms ?? 0;
		$result = [];
		for($i = 0; $i <= $terms; $i++) {
			if ($i != 0) {
				$grade = Grade::getGradeByOffset($curr_grade->id, $i * -1);
			} else {
				$grade = $curr_grade;
			}

			if (!$grade) {
				continue;
			}

			$scores = [];
			$exams  = [];
			foreach($students as $student) {
				$exam_scores = self::studentScores($student->id, $filter, $grade->id);
				if (!$exams) {
					$exams = $exam_scores['exams'];
				}
				$scores[$student->id] = $exam_scores;
			}
			$values = [];
			foreach($scores as $student_id => $score){
				$values = array_filter($score['scores'], function($v){
					return $v !== null;
				});
			}

			if (!empty($values)){
				$result[$grade->id] = [
					'students' => $students->keyBy('id')->all(),
					'exams'    => $exams->keyBy('id')->all(),
					'scores'   => $scores,
				];
			}
		}

		return $result;
	}

	public static function studentScores($student_id, $filter = [], $grade_id = null) {
		$student = Student::find($student_id);
		$grade   = empty($grade_id ?? '') ? $student->grade : Grade::find($grade_id);
		$exams   = Exam::findByFilter([
			'grade_level' => $grade->level,
			'subject_ids' => $filter['subject_id'] ?? [],
			'type'        => $filter['exam_type'] ?? null,
			'semester'    => $filter['exam_semester'] ?? null,
		]);
		if (!$exams) {
			return [];
		}

		$result = [
			'exams'  => $exams,
			'scores' => [],
		];
		foreach($exams as $exam) {
			$result['scores'][$exam->id] =
				Score::where('exam_id', $exam->id)
					->where('student_id', $student->id)
					->where('grade_id', $grade->id)
					->first();
		}

		return $result;
	}
}


