<?php

namespace App\Providers;

use App\Models\Subject;
use Illuminate\Database\Schema\Builder;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		Paginator::useBootstrap();
		Builder::defaultStringLength(191);
		setlocale(LC_TIME, env('APP_LOCALE', 'en_US').'.UTF-8');

		/* some views includes */
		Blade::include('includes.input', 'input');
		Blade::include('includes.score-value', 'score');

        if(\Schema::hasTable('subjects')){
            View::share('subjects', Subject::get()->keyBy(['id'])->all());
        }
		// View::share('grades', );
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
