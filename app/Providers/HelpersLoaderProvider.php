<?php

namespace App\Providers;

use App\Models\Setting;
use Blade;
use Config;
use Illuminate\Support\ServiceProvider;

use Schema;
use function date;
use function gmdate;
use function strtotime;

class HelpersLoaderProvider extends ServiceProvider {
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot() {
		Blade::directive('utc_datetime', function ($expression) {
			return date("Y-m-d\TH:i:sz", strtotime($expression));
		});
		Blade::directive('utc_date', function ($expression) {
			return date("Y-m-d\T00:00:00z", strtotime($expression));
		});
		Blade::directive('get_hours', function ($expression) {
			return gmdate('H', strtotime($expression));
		});
		Blade::directive('get_minutes', function ($expression) {
			return gmdate('i', strtotime($expression));
		});


	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {
		foreach (glob(app_path().'/Helpers/*.php') as $file) {
			require_once $file;
		}

	}
}
