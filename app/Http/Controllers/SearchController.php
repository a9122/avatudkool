<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use function abort;
use function response;

class SearchController extends Controller {
	private function _checkRequest(Request $request) {
		if (!$request->ajax()) {
			abort(404);
		}
	}

	private function _response($result, $msg = [], $data = []) {
		return response(['success' => $result, 'message' => $msg, 'data' => $data]);
	}

	public function checkIsikukood(Request $request) {
		$this->_checkRequest($request);

		$result = Student::validateIsikukood($request->code);

		return $this->_response($result, !$result ? 'Invalid code' : '');
	}
	/**
	 * Display a listing of the records.
	 * ?limit=20
	 * ?order=title,asc
	 * ?field=value -> where
	 * ?like=field,value
	 *
	 * @return \Illuminate\Http\Response
	 */
	private function _search(Request $request, $model, $with_relations=[]) {
		$limit = $request->all()['limit'] ?? 20;

		$order = $request->all()['order'] ?? null;
		if ($order !== null) {
			$order = explode(',', $order);
		}
		$order[0] = $order[0] ?? 'id';
		$order[1] = $order[1] ?? 'asc';

		$where = $request->all()['where'] ?? [];

		$like = $request->all()['like'] ?? null;
		if ($like) {
			$like = explode(',', $like);
			if (!isset($like[1])) {
				$like[0] = 'name';
				$like[1] = $like[0];
			}
			$like[1] = '%'.$like[1].'%';
		}

		$result = $model->orderBy($order[0], $order[1])
			->where(function($query) use ($like) {
				if ($like) {
					return $query->where($like[0], 'like', $like[1]);
				}

				return $query;
			})
			->where($where)
			->with($with_relations)
			->paginate($limit);

		return $this->_response(1, '', $results);
	}
	
	public function searchStudents(Request $request) {
		$this->_checkRequest($request);
		return $this->_search($request, Student::query(), ['scores']);
	}
}
