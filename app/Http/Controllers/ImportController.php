<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Grade;
use App\Models\Score;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;
use function compact;
use function file_read_csv;
use function implode;
use function storage_path;
use function time;
use function trim;
use function view;

class ImportController extends Controller {
	public function import(Request $request) {
		$request->validate([
			'file' => 'required|file|max:20480',
		]);
		$file_path      = $request->file('file')->storeAs('import_files', time().'.csv');
		$fpath     = storage_path('app/'.$file_path);

		$import_options = $request->options ?? ['import_all' => 1, 'validate_student_code' => 0, 'overwrite' => 1];

		$processed_rows_count = 0;
		$imported_count       = 0;
		$skipped_count        = 0;
		$rows_log             = [];
		$records_logs = [
			'subjects' => [],
			'grades'   => [],
			'students' => [],
			'exams'    => [],
			'scores'   => [],
		];
		file_read_csv($fpath, null,
			function($row, $idx) use (
				&$processed_rows_count, &$imported_count, &$skipped_count, &$records_logs, &$rows_log
			) {
				$processed_rows_count++;
				$err_msg = '';
				do {
					/* subject import */
					$subject      = null;
					$subject_name = $row['subject'] ?? '';
					if (!empty($subject_name)) {
						$subject = Subject::firstOrCreate(['name' => $subject_name]);
						if (!$subject) {
							$err_msg = 'Invalid Subject name';
							break;
						}
						$records_logs['subjects'][$subject->id] = $subject->name;
					} else {
						$err_msg = 'Empty Subject name';
						break;
					}

					$grade      = null;
					$grade_name = trim($row['grade'] ?? $row['classroom'] ?? '');
					if (!empty($grade_name)) {
						$grade = Grade::findOrCreate($grade_name);
						if (!$grade) {
							$err_msg = 'Invalid Grade name: '.$grade_name;
							break;
						}
						$records_logs['grades'][$grade->id] = $grade->name;
					} else {
						$err_msg = 'Empty Grade data';
						break;
					}

					$exam_data = [
						'grade_level' => $row['class'] ?: $grade->level,
						'subject_id'  => $subject->id,
						'type'        => $row['type'] ?? Exam::TYPE_SCHOOL,
						'semester'    => $row['semester'] ?? Exam::SEMESTER_AUTUMN,
					];

					$exam = Exam::firstOrCreate($exam_data);
					if (!$exam) {
						$err_msg = 'Invalid Exam data: '.implode('<br>', $exam_data);
						break;
					}
					$records_logs['exams'][$exam->id] = $exam->title;

					/* grade data import */
					$student      = null;
					$student_name = $row['student'] ?? '';
					$student_code = $row['isikukood'] ?? '';
					if (!empty($student_name) && !empty($student_code)) {
						$student = Student::getOrCreate($student_name, $student_code,
							$row['lang'] ?? 'est',
							$grade->id,
							$row['is active student'] ?? 1
						);
						if (!$student->wasRecentlyCreated) {

						}
						if (!$student) {
							$err_msg = 'Invalid Student data';
							break;
						}

						if ($student->grade->level < $grade->level) {
							$student->update(['grade_id' => $grade->id]);
						}

						$records_logs['students'][$student->id] = $student->full_name;
					} else {
						$err_msg = 'Empty Student data';
						break;
					}

					/* score data import */
					$score     = null;
					$score_val = $row['score'] ?? '';
					if (!empty($score_val)) {
						$score = Score::findOrCreate($exam, $student, $grade, $score_val, $row['date'] ?? now());
						if (!$score) {
							$err_msg = 'Invalid Score value: '.$score_val;
							break;
						}
						$records_logs['scores'][$score->id] = $score->id;

						$rows_log[$idx] = 'Row #'.$idx.' imported successful!';
					} else {
						$err_msg = 'Empty Score value';
						break;
					}

					$imported_count++;
					return true;
				} while (false);

				$skipped_count++;
				$rows_log[$idx] = 'Row #'.$idx.' importing error! '.$err_msg;

				return false;
			});

		return view('pages.import.result', [
			'total_count'    => $processed_rows_count,
			'imported_count' => $imported_count,
			'skipped_count'  => $skipped_count,
			'records_logs'   => $records_logs,
			'rows_log'       => $rows_log,
		]);
	}

	public function index(Request $request) {
		return view('pages.import.index');
	}
}
