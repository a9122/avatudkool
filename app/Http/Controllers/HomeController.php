<?php

namespace App\Http\Controllers;
use App\Models\Exam;
use App\Models\Grade;
use App\Models\Score;
use App\Models\Student;
use App\Models\Subject;
use App\Services\ScoreFinder;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use function back;
use function compact;
use function count;
use function implode;
use function in_array;
use function is_numeric;
use function view;

class HomeController extends Controller {

	/**
	 * Show search results.
	 *
	 * @param Request $request
	 *
	 * @return View
	 */
	public function search(Request $request) {
		if (!$request->filled('q')) {
			return back();
		}
		// Get the search keyword.

		$q = $request->input('q');

		Log::info("Search next keyword: `{$q}`");
		$students = Student::where(DB::raw('lower(first_name)'), 'like', '%'.mb_strtolower($q ?? '').'%')
			->orWhere(DB::raw('lower(last_name)'), 'like', '%'.mb_strtolower($q ?? '').'%')
			->orWhere('isikukood', 'like', '%'.mb_strtolower($q ?? '').'%')->get();
		return view('pages.home.search-result', compact('q', 'students'));
		// ['q' => $q, 'results' => $students]);
	}

	public function index(Request $request) {
		$students = Student::orderBy('grade_id')->orderBy('first_name')->get();
		$subjects = Subject::orderBy('name')->get();
		$exams    = Exam::orderBy('grade_level')->get();
		$grades   = Grade::withCount('students')->orderBy('level')->get();
		$stats    = [
			'students' => $students->count(),
			'subjects' => $subjects->count(),
			'grades'   => $grades->count(),
			'exams'    => $exams->count(),
		];

		return view('pages.home.index', compact('stats', 'students', 'grades', 'subjects'));
	}

	public function gradeScores(Request $request) {
		$grades     = Grade::find($request->grade_id);
		$offset = Grade::LEVEL_MAX; /* MAX period */
		if (!is_numeric($request->term)) {
			$offset = $grades[0]->level; /* as period */
			$term_label    = 'All time';
		} else {
			$offset_period = $request->term; /* as period */
			$term_label    = ($offset_period > 0) ? 'Last '.($request->term + 1).' years' : 'Last year';
		}
		$exam_type  = $request->exam_type;
		$type_label = in_array($exam_type, [Exam::TYPE_SCHOOL, Exam::TYPE_GOVERNMENT], true)
			? $exam_type.' only'
			: implode(' & ', [Exam::TYPE_SCHOOL, Exam::TYPE_GOVERNMENT]);
		$subject    = Subject::find($request->subject_id);

		$all_exams    = Exam::get()->keyBy(['id'])->all();
		$all_subjects = Subject::get()->keyBy(['id'])->all();
		$all_grades   = Grade::get()->keyBy(['id'])->all();
		$all_students = Student::get()->keyBy(['id'])->all();

		$dataset = [];
		foreach($grades as $grade) {
			$dataset[$grade->name] = ScoreFinder::gradeScores($grade->id, [
				'subject_id' => $subject['id'] ?? null,
				'exam_type'  => $exam_type,
			], $offset); /* [grade_id](0..N)->[student_id][exam_id] */
		}

		return $subject 
			? view('pages.home.parts._grade_subject_scores',
				compact('dataset', 
					'all_students', 'all_exams', 'all_grades', 'all_subjects',
					'term_label', 'type_label', 'subject'))
			: view('pages.home.parts._grade_scores',
				compact('dataset', 
					'all_students', 'all_exams', 'all_grades', 'all_subjects',
					'term_label', 'type_label', 'subject'));
	}

	public function studentScores(Request $request) {
		$student       = Student::find($request->student_id);
		$offset_period = Grade::LEVEL_MAX; /* MAX period */
		if (!is_numeric($request->term)) {
			$offset_period = $student->grade->level; /* as period */
			$term_label    = 'All time';
		} else {
			$offset_period = $request->term; /* as period */
			$term_label    = ($offset_period > 0) ? 'Last '.($request->term + 1).' years' : 'Last year';
		}
		$offset_years = $request->terms; /* as years */

		$curr_grade_id = $student->grade_id;
		$exam_type     = $request->exam_type;

		$type_label = in_array($exam_type, [Exam::TYPE_SCHOOL, Exam::TYPE_GOVERNMENT], true)
			? $exam_type.' only'
			: implode(' & ', [Exam::TYPE_SCHOOL, Exam::TYPE_GOVERNMENT]);

		$subject = [];
		if ($request->subject_id > 0) {
			$subject = Subject::find($request->subject_id);
		}
		$f__get_result = function($i) use ($student, $curr_grade_id, $exam_type, $request) {
			if ($i == 0) {
				$grade = $student->grade;
			} else {
				$grade = Grade::getGradeByOffset($curr_grade_id, $i * -1);
			}
			if (!$grade){
				return false;
			}
			if (in_array($exam_type, [Exam::TYPE_SCHOOL, Exam::TYPE_GOVERNMENT], true)) {
				$qry = Score::whereHas('exam', function(Builder $query) use ($exam_type) {
					$query->where('type', $exam_type);
				})->where('student_id', $student->id)->where('grade_id', $grade->id);
			} else {
				$qry = Score::where('student_id', $student->id)->where('grade_id', $grade->id);
			}

			if ($request->subject_id > 0) {
				$qry = $qry->whereHas('exam', function(Builder $query) use ($request) {
					$query->where('subject_id', $request->subject_id);
				});
			}

			return ['grade_name' => $grade->name, 'scores' => $qry->orderBy('exam_id')->with('exam')->get()];
		};

		$scores = [];
		if ($request->term_type === 'years') {
			foreach($offset_years as $i) {
				$res = $f__get_result($i);
				if ($res && count($res['scores'])) {
					$scores[$res['grade_name']] = $res['scores'] ?? [];
				}
			}
		} else {
			for($i = 0; $i <= $offset_period; $i++) {
				$res = $f__get_result($i);
				if ($res && count($res['scores'])) {
					$scores[$res['grade_name']] = $res['scores'] ?? [];
				}
			}
		}

		return view('pages.home.parts._student_scores',
			compact('scores', 'student', 'subject', 'term_label', 'type_label', 'offset_period')
		);
	}


}
