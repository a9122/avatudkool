<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Grade;
use App\Models\Score;
use App\Models\Student;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function view;

class ScoreController extends Controller {
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(Request $request) {
		$grades   = Grade::get()->keyBy('id');
		$students = Student::get()->keyBy('id');
		$exams    = Exam::get()->keyBy('id');

		return view('pages.scores.create', compact('students', 'exams'));
	}

	public function index() {
		$list = Score::paginate(20);

		return view('pages.scores.index', compact('list'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'student_id' => 'required',
			'exam_id'    => 'required',
			'score'      => 'required|numeric|min:0|max:100'
		]);
		$student = Student::find($request->student_id);
		$exam = Exam::find($request->exam_id);
		$grade = Grade::where('level', $exam->grade_level)->where('group', $student->grade->group)->first();
		$data = [
			'student_id' => $request->student_id,
			'exam_id'    => $request->exam_id,
			'grade_id'   => $grade->id
		];
		$score = Score::firstOrCreate($data,[
			'date'	     => date('Y-m-d', strtotime($request->date ?? now())),
			'score'	     => $request->score
		]);
		if (!$score->wasRecentlyCreated){
			$score->update([
				'date'	     => now(),
				'score'	     => $request->score
			]);
			return redirect()->route('scores.index')->with('toast.success', 'Score updated!');
		}
		return redirect()->route('scores.index')->with('toast.success', 'Score created!');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id) {
		$rec = Score::find($id);
		$grades   = Grade::get()->keyBy('id');
		$students = Student::get()->keyBy('id');
		$exams    = Exam::get()->keyBy('id');

		return view('pages.scores.edit', compact('students', 'exams', 'rec'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'student_id' => 'required',
			'exam_id'    => 'required',
			'score'      => 'required|numeric|min:0|max:100'
		]);
		$student = Student::find($request->student_id);
		$exam = Exam::find($request->exam_id);
		$grade = Grade::where('level', $exam->grade_level)->where('group', $student->grade->group)->first();
		$data = [
			'student_id' => $request->student_id,
			'exam_id'    => $request->exam_id,
			'grade_id'   => $grade->id
		];
		$score = Score::firstOrCreate($data,[
			'date'	     => date('Y-m-d', strtotime($request->date ?? now())),
			'score'	     => $request->score
		]);
		if (!$score->wasRecentlyCreated){
			$score->update([
				'date'	     => date('Y-m-d', strtotime($request->date ?? now())),
				'score'	     => $request->score
			]);
			return redirect()->route('scores.index')->with('toast.success', 'Score updated!');
		}
		return redirect()->route('scores.index')->with('toast.success', 'Score created!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		$rec = Score::findOrFail($id);
		$rec->delete();

		return redirect()->route('scores.index')->with('toast.success', 'Score deleted!');
	}

	/**
	 * Display a detail info of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(Request $request, $id) {
		$rec = Score::find($id);
		if (!$rec) {
			return redirect()->back()->with('toast.error', 'Score not found!');
		}
		$readonly = true;

		return view('pages.scores.view', compact('rec', 'readonly'));
	}
}
