<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use function __;
use function array_filter;
use function compact;
use function current_user;
use function redirect;
use function view;

class AccountController extends Controller {
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(Request $request) {
		if (!current_user()->is_admin()) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}

		return view('pages.accounts.create', ['force_teacher' => $request->get('t', false)]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		if (!current_user()->is_admin()) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}
		$user = User::find($id);
		if (!$user) {
			return redirect()->back()->with('toast.error', 'Account not found')->send();
		}
		$name = $user->name;
		$user->delete();

		return redirect()->route('accounts.index')->with('toast.success', 'User "'.$name.'" deleted');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id) {
		if (!current_user()->is_admin()) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}
		$user = User::find($id);
		if (!$user) {
			return redirect()->back()->with('toast.error', 'User not found');
		}

		return view('pages.accounts.profile', compact('user'));
	}

	public function index(Request $request) {
		if (!current_user()->is_admin()) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}
		$filter = array_filter($request->get('filter', []));
		$list = $this->_filterData($filter);

		return view('pages.accounts.index', compact('list', 'filter'));
	}

	/**
	 * Display a detail info of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(Request $request, $id) {
		$is_owner = (current_user()->id == $id);
		if (!current_user()->is_admin() && ($is_owner)) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}
		$user = User::find($id);
		if (!$user) {
			return redirect()->back()->with('toast.error', 'User not found');
		}

		return view('pages.accounts.profile', compact('user', 'is_owner'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request) {
		if (!current_user()->is_admin()) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}
		$request->validate([
			'name'     => 'required|unique:App\Models\User,name',
			'email'    => 'required|email|unique:App\Models\User,email',
			'password' => ['required', 'string', 'min:6', 'confirmed'],
		]);
		$data             = $request->all();
		$data['password'] = Hash::make($data['password']);

		$rec = new User($data);
		$rec->save();

		return redirect()->back()->with('toast.success', 'User created!');
	}

	public function indexTeachers(Request $request) {
		$filter = array_filter($request->get('filter', []));
		$list = $this->_filterData($filter, true);

		return view('pages.accounts.index', compact('list', 'filter'))->with(['title' => 'Teachers']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param                          $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'name'     => 'required',
			'password' => 'sometimes|string|min:8|confirmed',
			// ['required', 'string', 'min:8', 'confirmed'],
			'email'    => 'sometimes|nullable|email|unique:App\Models\User,email,'.$id,
		]);
		$data = $request->all();
		if ($request->filled('password')) {
			$data['password'] = Hash::make($data['password']);
		}
		$user = User::find($id);

		if (!current_user()->is_admin() && (current_user()->id == $id)) {
			return redirect()->back()->with('toast.error', __('app.need_admin_permissions'))->send();
		}
		if (!$user) {
			$user = new User($request->all());
			$user->save();
		} else {
			$user->update($request->all());
		}

		return redirect()->back()->with('toast.success', 'User saved!');
	}

	private function _filterData($filter, $only_teacher = false) {

		if ($only_teacher) {
			$query = User::query()->where('type', '=', 'teacher');
		} else {
			$query = User::query()->where('id', '!=', current_user()->id);
		}

		if (!empty($filter)) {
			foreach($filter as $k => $v) {
				if ($k === 'q') {
					$query->where('name', 'like', '%'.$v.'%')->orWhere('email', 'like', '%'.$v.'%');
				} else {
					$query->where($k, $v);
				}
			}
		}

		return $query->orderBy('name', 'desc')->paginate(12);
	}
}
