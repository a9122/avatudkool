<?php

namespace App\Http\Controllers;
use App\Models\Grade;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function view;

class GradeController extends Controller {
	public function index() {
		$list = Grade::withCount('students')->paginate(20);

		return view('pages.grades.index', compact('list'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		return view('pages.grades.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'level' => 'required|numeric|min:1|max:9',
			'group' => 'required|string|min:1|max:20',
		]);
		Grade::create($request->all());

		return redirect()->route('grades.index')->with('toast.success', 'Grade created!');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id) {
		$rec = Grade::find($id);

		return view('pages.grades.edit', ['rec' => $rec]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'level' => 'required|numeric|min:1|max:9',
			'group' => 'required|string|min:1|max:20',
		]);
		$rec = Grade::findOrFail($id);
		$rec->update($request->all());

		return redirect()->route('grades.index')->with('toast.success', 'Grade saved!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		$rec = Grade::find($id);
		if ($rec->students->count()) {
			return redirect()->route('grades.index')->with('toast.error', 'Grade has students!');
		}
		$rec->delete();

		return redirect()->route('grades.index')->with('toast.success', 'Grade deleted!');
	}

	/**
	 * Display a detail info of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(Request $request, $id) {
		$rec = Grade::find($id);
		if (!$rec) {
			return redirect()->back()->with('toast.error', 'Grade not found!');
		}

		return view('pages.grades.view', compact('rec'));
	}

}
