<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Exam;
use App\Models\Grade;
use App\Models\Score;
use App\Models\Student;
use Carbon\Carbon;
use function current_user;
use function redirect;
use function time;

class ClassroomController {
	public function graduate(Request $request, $grade) {
		$key             = $request->promotion_key ?? null;
		$from_grade      = Grade::find($grade);
		$students        = $from_grade->getStudents($key);
		$grade_promotion = time();

		$students->update([
			'grade_id'        => null,
			'status'          => 2,
			'graduated_at'    => Carbon::now(),
			'grade_promotion' => $grade_promotion,
		]);

		return redirect()->back()
			->with('toast.success', 'Students from '.$from_grade->name.'<br> graduated successfully!');
	}

	public function promote(Request $request, $grade, $to_grade = null) {
		$key = $request->promotion_key ?? null;
		$from_grade = Grade::findOrFail($grade);
		$target_students = $from_grade->getStudents($key)->get();
		if ($to_grade){
			$to_grade = Grade::findOrFail($to_grade);
		} else {
			$to_grade = Grade::firstOrCreate([
				'level'   => $from_grade->level + 1,
				'group'   => $from_grade->group,
				'user_id' => current_user()->id,
			]);
		}
		$grade_promotion = time();
		foreach($target_students as $target_student) {
			$target_student->update(['grade_id' => $to_grade->id, 'grade_promotion' => $grade_promotion]);
		}

		return redirect()->route('school.classroom.scores', ['grade' => $to_grade->id])
			->with('toast.success', 'All students promoted successfully!');
	}

	public function scores(Request $request, $grade_id) {
		$grade = Grade::find($grade_id);
		if (!$grade) {
			abort(404);
		}
		$next_grade = $grade->next_grade;
		$list       = Student::active()->where('grade_id', $grade_id)->get()->groupBy('grade_promotion');
		$exams      = Exam::getAllByGrade($grade_id);
		$scores     = [];
		foreach($list as $key => $students) {
			if(empty($key)){
				$key = 0;
			}
			/** @var Student $student */
			foreach($students as $student) {
				$exams_scores = [];
				foreach($exams as $exam) {
					$score_value =
						Score::where('exam_id', $exam->id)
							->where('student_id', $student->id)
							->where('grade_id', $grade_id)
							->first();
					$exams_scores[$exam->id] = $score_value;
					$scores[$key][$student->id][$exam->id] = $score_value;

				}
				$student->setAttribute('exams_scores', $exams_scores);
			}
		}

		$pk  = Student::active()->where('grade_id', $grade_id)->get()->distinct('grade_promotion');
		return view('pages.classroom', compact('grade', 'pk', 'next_grade', 'list', 'exams', 'scores'))
			->with('toast.success', 'Students from '.$grade->name.'<br> graduated successfully!');
	}
}


