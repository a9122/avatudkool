<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function view;

class SubjectController extends Controller {
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		return view('pages.subjects.create');
	}

	/**
	 * Display a detail info of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(Request $request, $id) {
		$rec = Subject::find($id);
		if (!$rec) {
			return redirect()->back()->with('toast.error', 'Subject not found!');
		}
		$scores = [];

		return view('pages.subjects.view', compact('rec'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		$rec = Subject::find($id);
		if ($rec->exams->count()) {
			return redirect()->route('subjects.index')->with('toast.error', 'Subject using in exams!');
		}
		$rec->delete();

		return redirect()->route('subjects.index')->with('toast.success', 'Subject deleted!');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id) {
		$rec = Subject::find($id);

		return view('pages.subjects.edit', ['rec' => $rec]);
	}

	public function index() {
		$list = Subject::paginate(10);

		return view('pages.subjects.index', compact('list'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'name'        => 'required|unique:App\Models\Subject,name|min:2'
		]);
		Subject::create($request->all());

		return redirect()->route('subjects.index')->with('toast.success', 'Subject created!');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'name'        => 'required|min:2|unique:App\Models\Subject,name,'.$id
		]);
		$rec = Subject::find($id);
		$rec->update($request->all());

		return redirect()->route('subjects.index')->with('toast.success', 'Subject saved!');
	}
}
