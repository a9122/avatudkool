<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use App\Models\Score;
use App\Models\Student;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function view;

class StudentController extends Controller {

	public function create(Request $request) {
		return view('pages.students.create', ['params' => $request->all(), 'grades' => Grade::get()->keyBy(['id'])->all()]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		$rec = Student::find($id);
		if (!$rec) {
			return redirect()->back()->with('error', 'Record not found')->send();
		}
		$name = $rec->name;
		$rec->delete();

		return redirect()->route('students.index')->with('toast.success',
			'Student "'.$name.'" deleted');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id) {
		$rec = Student::findOrFail($id);
		if (!$rec) {
			return redirect()->back()->with('error', 'Supplier not found');
		}
		$grades = Grade::get()->keyBy(['id'])->all();
		return view('pages.students.edit', compact('rec', 'grades'));
	}

	public function index(Request $request) {
		$list = Student::orderBy('first_name')->orderBy('last_name')->paginate(20);

		return view('pages.students.index', compact('list'));
	}

	/**
	 * Display a detail info of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(Request $request, $id) {
		$rec = Student::findOrFail($id);
		if (!$rec) {
			return redirect()->back()->with('toast.error', 'Student not found!');
		}
		$offset_period = $rec->grade->level;
		$curr_grade_id = $rec->grade_id;
		$subject_id = null;
		$student       = $rec;
		$f__get_result = function($i) use ($student, $curr_grade_id, $subject_id) {
			if ($i === 0) {
				$grade = $student->grade;
			} else {
				$grade = Grade::getGradeByOffset($curr_grade_id, $i * -1);
			}
			if (!$grade) {
				return false;
			}

			$qry = Score::where('student_id', $student->id)->where('grade_id', $grade->id);

			if ($subject_id) {
				$qry = $qry->whereHas('exam', function(Builder $query) use ($subject_id) {
					$query->where('subject_id', $subject_id);
				});
			}

			return ['grade_name' => $grade->name, 'scores' => $qry->orderBy('exam_id')->with('exam')->get()];
		};
		$scores = [];

		for($i = 0; $i <= $offset_period; $i++) {
			$res = $f__get_result($i);
			if ($res) {
				$scores[$res['grade_name']] = $res['scores'] ?? [];
			}
		}

		return view('pages.students.view', compact('rec', 'scores'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'first_name' => 'required',
			'last_name'  => 'required',
			'isikukood'  => 'required|unique:App\Models\Student,isikukood|numeric',
			'lang'       => 'required',
			'grade_id'   => 'required',
		]);

		$rec = new Student($request->all());
		$rec->save();

		return redirect()->route('students.index')->with('toast.success', 'Student added!');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param                          $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'first_name' => 'required',
			'last_name'  => 'required',
			'isikukood' => 'required|numeric|unique:App\Models\Student,isikukood,'.$id,
			'lang'       => 'required',
			'grade_id'   => 'required',
		]);
		$rec = Student::findOrFail($id);
		if (!$rec) {
			// return redirect()->back()->with('error', 'Supplier not found');
			$rec = new Student($request->all());
			$rec->save();
		} else {
			$rec->update($request->all());
		}

		return redirect()->route('students.index')->with('toast.success', 'Student saved!');
	}
}
