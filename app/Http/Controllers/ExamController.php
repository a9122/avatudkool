<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function view;

class ExamController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		return view('pages.exams.create');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		$rec = Exam::find($id)->withCount('scores');
		if ($rec->scores_count) {
			return redirect()->route('exams.index')->with('toast.error', 'Exam record using!');
		}
		$rec->delete();

		return redirect()->route('exams.index')->with('toast.success', 'Exam deleted!');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id) {
		$rec = Exam::findOrFail($id);

		return view('pages.exams.edit', ['rec' => $rec]);
	}

	public function index() {
		$list = Exam::paginate(10);

		return view('pages.exams.index', compact('list'));
	}

	/**
	 * Display a detail info of the resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show(Request $request, $id) {
		$rec = Exam::findOrFail($id);
		if (!$rec) {
			return redirect()->back()->with('toast.error', 'Exam not found!');
		}

		return view('pages.exams.view', compact('rec'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'subject_id'  => 'required',
			'type'        => 'required',
			'semester'    => 'required',
			'grade_level' => 'required|numeric|min:1|max:9',
		]);
		Exam::create($request->all());

		return redirect()->route('exams.index')->with('toast.success', 'Exam created!');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
		$request->validate([
			'subject_id'  => 'required',
			'type'        => 'required',
			'semester'    => 'required',
			'grade_level' => 'required|numeric|min:1|max:9',
		]);
		$rec = Exam::findOrFail($id);
		$rec->update($request->all());

		return redirect()->route('exams.index')->with('toast.success', 'Exam saved!');
	}

}
