@if(false && current_user()->is_admin())
    <a class="btn-link btn-sm open-modal-editor"
        data-editor="score"
        data-student_id="{{ $student_id ?? 0 }}"
        data-exam_id="{{ $exam_id ?? 0 }}"
        data-grade_id="{{ $student_id ?? 0 }}"
        data-score_id="{{ $score_id ?? 0}}"
        title="Click to edit"
    >
        {{ $score_value ?? '+ add' }}
    </a>
@else
    {{ $score_value ?? ' ' }}
@endif