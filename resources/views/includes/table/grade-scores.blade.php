<?php
$summary       = [];
$exams    = $grade_scores['exams'] ?? [];
$students = $grade_scores['students'] ?? [];
$scores   = $grade_scores['scores'] ?? [];
?>
<div class="table-responsive">
	<table class="table table-bordered table-hover text-nowrap grade-scores table-sm" style="font-size:14px">
		@include('includes.table.thead-exams')
		<tbody>
		@if (count($students))
			@foreach($students as $student)
				<tr>
					<td class="bg-gradient-lightblue p-2" style="white-space: nowrap;">
						{{ $student->full_name }}
					</td>
					@foreach($exams as $exam)
						@php
							$score = $scores[$student->id]['scores'][$exam->id] ?? null;
							$summary[$exam->id] = $summary[$exam->id]?? array_combine(array_merge(['total'], App\Models\Score::COLORS), [0, 0, 0, 0]);
							if ($score){
								$summary[$exam->id][$score->color]++;
							}
							$summary[$exam->id]['total']++;
						@endphp
						@if (isset($score))
							<td class="text-center" style="color:#fff;background:{{$score->color}}">
								@include('includes.score-value',['student_id'=>$student->id, 'score_id'=>$score->id, 'exam_id'=>$exam->id, 'grade_id'=>$grade_id, 'score_value'=>$score->score])
							</td>
						@else
							<td>
								@include('includes.score-value',['student_id'=>$student->id,'exam_id'=>$exam->id, 'grade_id'=>$grade_id])
							</td>
						@endif
					@endforeach
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="{{count($exams) + 1}}">
					<p class="text-gray text-lg">Nothing found...</p>
				</td>
			</tr>
		@endif
		</tbody>
		@if($show_summary ?? true)
			@include('includes.table.tfoot-exams')
		@endif
	</table>
</div>