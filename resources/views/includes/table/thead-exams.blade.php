<?php

$exam_subjects = [];
$prev_subj_id  = null;
/** @var \App\Models\Exam $exam */
foreach($exams ?? [] as $exam):
	$exam_subjects[$exam->subject_id] = $exam_subjects[$exam->subject_id]
		?? ['colspan' => 1, 'text' => $exam->subject->name];
	if ($prev_subj_id == $exam->subject_id) {
		$exam_subjects[$exam->subject_id]['colspan']++;
	}
	$prev_subj_id = $exam->subject_id;
endforeach;

?>
<thead>
<tr>
    <th class="transparent" style="width:20%;" rowspan="2">
        {{ $all_grades[$grade_id]['name'] }}
    </th>
    @foreach($exam_subjects as $subj_id => $subj_data)
        <th class="text-center bg-gradient-purple p-2"

            colspan="{{ $subj_data['colspan'] }}">
            {{ $subj_data['text'] }}
            <br>
        </th>
    @endforeach
</tr>
<tr>
    <th class="transparent hidden" style="width:20%;"></th>
    @foreach($exams as $exam)
        <th class="text-center bg-gray-light p-1"
            style="width:{{ (int)(80 / count($exams)) }}%">
            <span class="font-weight-normal text-sm">{{ $exam->type }}, {{ $exam->semester }}</span>
        </th>
    @endforeach
</tr>
</thead>
