<?php

?>
@if($summary)
    <tfoot>
    <tr>
        <td class="transparent" style="padding: 4px 0;"></td>
        @foreach($exams as $exam)
            <td style="padding: 4px 0;">
                <div class="position-relative mb-1 chart-block"
                    data-c_red="{{$summary[$exam->id][App\Models\Score::COLOR_RED] ?? 0}}"
                    data-c_green="{{$summary[$exam->id][App\Models\Score::COLOR_GREEN] ?? 0}}"
                    data-c_yellow="{{$summary[$exam->id][App\Models\Score::COLOR_YELLOW] ?? 0}}"

                    data-p_red="{{($summary[$exam->id][App\Models\Score::COLOR_RED] ?? 0)}}"
                    data-p_green="{{($summary[$exam->id][App\Models\Score::COLOR_GREEN] ?? 0)}}"
                    data-p_yellow="{{($summary[$exam->id][App\Models\Score::COLOR_YELLOW] ?? 0)}}"

                    data-total="{{($summary[$exam->id]['total'] ?? 0)}}">
                    <canvas class="summary-chart" width="140" height="140"></canvas>
                    <div class="summary-legend p-1 m-auto mt-2"></div>
                </div>
            </td>
        @endforeach
    </tr>
    </tfoot>
@endif
