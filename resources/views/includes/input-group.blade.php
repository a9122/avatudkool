<div class="input-group {{ ($class ?? '') }}">
    <div class="input-group-prepend"
    @if(!empty($opts['required']??''))
        title="Field is required"
    @endif
    >
        <span class="input-group-text" style="min-width:110px;">
            @if(!empty($icon??''))
                <i class="{{$icon}} {{ !empty($label ?? '') ? 'mr-1' : ''}}"></i>{{ ($label ?? '') }}
            @else
                {{ trim($label ?? mb_ucfirst($name ?? '')) }}
            @endif
            @if(!empty($opts['required']??''))
                <code class="text-danger ml-1">*</code>
            @endif
        </span>
    </div>
    <input type="{{ ($type ?? 'text') }}"
        name="{{ $name }}"
        value="{{ $value }}"
        placeholder="Enter {{ ($label ?? $name ?? 'value') }} "
        class="form-control @error($name) is-invalid @enderror"
        {!! html_attributes($opts ?? [], ['name', 'class', 'value', 'type']) !!}
    >
    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
