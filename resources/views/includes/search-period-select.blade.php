@php
    $curr_year = date('Y');

@endphp
<div class="form-group input-group-sm search-term">
    <div class="row ml-1" style="margin-bottom:6px">
        <div class="form-check col-sm-6">
            <input
                class="form-check-input term-switch"
                type="radio"
                id="{{ ($pref ?? '').'term-switch-period' }}"
                value="period"
                name="term_type"
                checked>
            <label class="form-check-label align-middle"
                for="{{ ($pref ?? '').'term-switch-period' }}">
                Study period
            </label>
        </div>
        <div class="form-check col-sm-6">
            <input
                class="form-check-input term-switch"
                id="{{ ($pref ?? '').'term-switch-years' }}"
                type="radio"
                value="years"
                name="term_type">
            <label class="form-check-label align-middle"
                for="{{ ($pref ?? '').'term-switch-years' }}">
                Study year(-s)
            </label>
        </div>
    </div>
    <div class="input-group-sm term-selector years hidden">
        <select name="terms[]" class="select2 term" multiple>
            @for($i=0;$i<9;$i++)
                <option value="{{ $i }}" {{ $i === 0 ? 'selected' : '' }}>
                    {{ $curr_year-($i+1) }}/{{ $curr_year-$i }}
                </option>
            @endfor
        </select>
    </div>
    <div class="input-group-sm term-selector period">
        <select name="term" class="custom-select term">
            <option value="0">Latest year</option>
            @for($i=0;$i<9;$i++)
                <option value="{{ $i+1 }}">Last {{ $i+2 }} years</option>
            @endfor
            <option value="all">All time</option>
        </select>
    </div>
</div>
