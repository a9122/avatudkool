<div class="list-group-item list-group-item-action">
    <div class="row">
        <div class="col-auto">
            <i class="fas fa-user-clock fa-3x text-gray"></i>
        </div>
        <div class="col px-4">
            <div>
                <h3>
                    <a href="{{route('students.show', ['student'=>$student->id])}}"
                        title="Show student card" class="text-gray">{{ $student->full_name }}</a>
                    <a href="{{route('school.classroom.scores',['grade'=> $student->grade->id])}}"
                        title="Show grade students">
                        <span class="float-right text-lg font-weight-light">{{ $student->grade->name }}</span>
                    </a>
                </h3>
                <ul class="list-group mb-1">
                    <li class="list-group-item">
                        <b>Isikukood: </b>
                        <span class="text-primary float-right">{{ $student->isikukood }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Lang: </b>
                        <span class="text-primary float-right">{{ $student->language }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>