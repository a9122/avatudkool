@extends('adminlte::page')

@section('title', ' | '.ucfirst($rec->name))

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Grade {{ $rec->name }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('grades.index') }}">Grades</a></li>
                        <li class="breadcrumb-item active">{{ $rec->name }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section><!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-success">
                        <!-- /.card-header -->
                        @include('pages.grades.parts.form', ['rec'=>$rec, 'readonly'=>true])
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/datatables/datatables.min.css')}}">
    <style>
		input.form-control:disabled, input.form-control[readonly] {
			border : none;
		}
		.form-control:disabled, .form-control[readonly] {
			background : transparent;
		}
    </style>
@stop
