@extends('adminlte::page')

@section('title', ' | Grades')

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card card-warning card-outline">
				<div class="card-header d-flex">
					<h3 class="card-title p-1">
						<i class="fas fa-chalkboard-teacher"></i> Grades </h3>
				</div>
				<div class="card-body">
					@if(current_user()->is_admin())
						<a href="{{ route('grades.create') }}" class="btn btn-primary mb-3">+ create new</a>
					@endif

					@if ($list->links())
						<div class="float-right">
							{{ $list->links() }}
						</div>
					@endif
					<div class="table-responsive">
						<table id="donors" class="table table-bordered table-hover text-nowrap" style="font-size:12px">
							<thead>
							<tr>
								<th style="width: 30px; text-align: center">#</th>
								<th class="text-primary">Name</th>
								<th>Info</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							@if (count($list))
								@foreach($list as $rec)
									<tr>
										<td class="bg-lightblue text-center">
											{{ ($list->firstItem() + $loop->index) }}
										</td>
										<td style="white-space: normal;">
											<p style="margin-bottom: 6px;font-size: 14px"><b>{{ $rec->name }}</b></p>

											{{$rec->students_count}} students
										</td>
										<td class="tbl-col-project">
											{{ $rec->description ?? '-' }}
										</td>
										<td class="text-center" style="width:13%;">
											<a href="{{ route('school.classroom.scores', ['grade' => $rec->id]) }}"
												target="_blank" class="btn btn-primary btn-sm float-left mr-1">
												<i class="fas fa-eye"></i> </a>
											@if(current_user()->is_admin())

												<a href="{{ route('grades.edit', ['grade' => $rec->id]) }}" class="btn btn-info btn-sm float-left mr-1">
													<i class="fas fa-pencil-alt"></i> </a>

												<form action="{{ route('grades.destroy', ['grade' => $rec->id]) }}" method="post" class="float-left">
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Confirm delete grade?')">
														<i class="fas fa-trash-alt"></i>
													</button>
											</form>
											@endif
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="4">
										<p class="text-gray text-lg">nothing found...</p>
									</td>
								</tr>
							@endif

							</tbody>
						</table>
					</div>
					@if ($list->links())
						<h4 class="float-left">Showing {{$list->firstItem()}} to {{$list->lastItem()}} of {{$list->total()}} entries </h4>
						<div class="float-right">
							{{ $list->links() }}
						</div>
					@endif
				</div>
				<!-- /.card-body -->
			</div>
		</div>
	</div>
@stop