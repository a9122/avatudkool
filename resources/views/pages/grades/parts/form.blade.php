<form role="form" method="post"
    action="{{ isset($rec) ? route('grades.update', ['grade' => $rec->id]) : route('grades.store')}}">
    @if($readonly ?? false)
    @else
        @csrf
        @if(isset($rec))
            @method('PUT')
        @else
            @method('POST')
        @endif
    @endif
    <div class="card-body">
        <div class="form-group">
            @include('includes.input-group', ['name' => 'level', 'type'=>'number', 'value'=>old('name', $rec['level'] ?? ''), 'opts'=>['minlength'=>2, 'required'=>true, 'min'=>1, 'max'=>9, 'readonly'=> ($readonly ?? false)]])
        </div>
        <div class="form-group">
            @include('includes.input-group', ['name' => 'group', 'value'=>old('name', $rec['group'] ?? ''), 'opts'=>['minlength'=>2, 'required'=>true, 'readonly'=> ($readonly ?? false)]])
        </div>
        <div class="form-group">
            <hr>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" {{($readonly ?? false) ? 'readonly' : ''}}
            class="form-control @error('description') is-invalid @enderror" id="description" rows="4">{{ old('description', $rec['description'] ?? '') }}</textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="card-footer">
        @if(!$readonly)
            <button type="submit" class="btn btn-primary col-sm-4 float-right">Save</button>
        @endif
        <a href="{{ route('grades.index') }}" class="btn btn-default col-sm-2 float-right">Back</a>
    </div>
</form>