@extends('adminlte::page')

@section('title', ' | Class '.$grade['level'].$grade['group'])

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Classroom
						<span class="text-primary">{{ $grade['name'] }}</span>
					</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/">Classroom</a></li>
						<li class="breadcrumb-item active">{{ $grade['name'] }}</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="card card-info card-outline">
			<div class="card-body pb-1">
				@if($grade['description'] ?? false)

					<div class="row">
						<div class="col-md-12">
							<div class="callout callout-info">
								<h5 class="mb-0"><i class="fas fa-chalkboard"></i>
									<span class="lead">{{ nl2br($grade['description']) }}</span>
								</h5>

							</div>
						</div>
					</div>

				@endif
				@if(count($list))
					<hr class="m-0 mt-2 mb-2">
					@foreach($list as $group_key => $students)
						@php
							if(empty($group_key)) {
                                $group_key = 0;
                            }
						@endphp
						<div class="info-box bg-light">
							<div class="info-box-content">

								<div class="row">
									@if($group_key)
										<div class="offset-6 col-md-6 text-right">
											<p class="text-muted">Promoted at {{ date('d.m.Y', $group_key) }} </p>
										</div>
									@endif
									<div class="col-md-12 table-responsive">
										<table class="table table-bordered table-hover text-nowrap" style="font-size:13px">
											<thead>
											<tr>
												<th style="width: 30px; text-align: center">#</th>
												<th class="text-primary" style="width:160px">Student</th>
												@foreach($exams as $exam)
													<th class="text-center" style="width:160px">
														{{ $exam->subject->name }}<br>
														<small>{{ $exam->semester }}</small>
													</th>
												@endforeach
											</tr>
											</thead>
											<tbody>
											@if (count($students))
												@foreach($students as $student)
													<tr>
														<td class="bg-lightblue text-center">
															{{ $loop->index + 1 }}
														</td>
														<td style="white-space: normal;padding: 8px 4px;">
															<a href="{{ route('students.show', ['student' => $student['id']]) }}">{{ $student['full_name'] }}</a>
														</td>
														@foreach($exams as $exam)
															@php
																$score = $student->exams_scores[$exam->id] ?? null;
															@endphp
															@if (isset($score))
																<td class="text-center"
																	style="color:#fff;background:{{$score->color}}">
																	{{ $score->score }}
																</td>
															@else
																<td> -</td>
															@endif
														@endforeach
													</tr>
												@endforeach
											@else
												<tr>
													<td colspan="{{count($exams) + 2}}">
														<p class="text-gray text-lg">Students not found...</p>
													</td>
												</tr>
											@endif
											</tbody>
											<tfoot>
											<tr>
												<td colspan="2">

												</td>
												<td class="" colspan="{{count($exams)}}">
													<div class="row text-right">
														<div class="offset-6"></div>
														@if($grade['level'] < \App\Models\Grade::LEVEL_MAX)
															<form
																class="col-md-3"
																action="{{ route('school.classroom.promote', ['grade' => $grade['id']]) }}"
																method="post">
																@csrf
																<input type="hidden" value="{{ $group_key }}" name="promotion_key">
																<button type="submit"
																	class="btn btn-outline-primary btn-sm btn-block"
																	onclick="return confirm('Promote students to the next grade?')">
																	<i class="fas fa-angle-double-right"></i> Promote to next grade
																</button>
															</form>
														@else
														@endif
														<form
															class="col-md-3"
															action="{{ route('school.classroom.graduate', ['grade' => $grade['id']]) }}"
															method="post">
															@csrf
															<input type="hidden" value="{{ $group_key }}" name="promotion_key">
															<button type="submit" class="btn btn-outline-warning btn-sm btn-block"
																onclick="return confirm('Promote students to the next grade?')">
																<i class="fas fa-user-graduate"></i> Graduate students
															</button>
														</form>
													</div>
												</td>
											</tr>
											</tfoot>
										</table>
									</div>
								</div>

							</div>
						</div>
					@endforeach
				@else
					<div class="info-box bg-light">
						<div class="info-box-content">
							<span class="info-box-text text-center text-muted lead">There are no students in this class yet</span>
						</div>
					</div>
				@endif
			</div>
			<!-- /.card-body -->
			<div class="card-footer">

			</div>
			<!-- /.card-footer -->
		</div>
		<!-- /.card -->

	</section>
@stop