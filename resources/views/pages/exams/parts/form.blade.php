<?php

?>
<form role="form" method="post"
    action="{{ isset($rec) ? route('exams.update', ['exam' => $rec->id]) : route('exams.store')}}">
    @if($readonly ?? false)
    @else
        @csrf
        @if(isset($rec))
            @method('PUT')
        @else
            @method('POST')
        @endif
    @endif
    <div class="card-body">


        <div class="form-group row">
            <div class="input-group col-md-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Subject
                    </span>
                </div>
                <select class="custom-select {{($readonly ?? false) ? 'disabled' : ''}} @error('subject_id') is-invalid @enderror"
                    id="subject_id" name="subject_id" required>
                    @foreach($subjects as $k => $v)
                        <option value="{{ $k }}" @if($k == (old('subject_id', $rec['subject_id']) ?? '')) selected @endif>
                            {{ $v['name'] }}
                        </option>
                    @endforeach
                </select>
                @error('subject_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group col-md-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Grade level
                    </span>
                </div>
                <input type="number" name="grade_level" {{($readonly ?? false) ? 'readonly' : ''}}
                value="{{ old('grade_level', $rec['grade_level'] ?? '1') }}"
                    class="form-control @error('grade_level') is-invalid @enderror"
                    min="1" max="9" required>
                @error('grade_level')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <div class="input-group col-md-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Type
                    </span>
                </div>
                <select class="custom-select {{($readonly ?? false) ? 'disabled' : ''}} @error('type') is-invalid @enderror" {{($readonly ?? false) ? 'readonly' : ''}} id="type" name="type">
                    @foreach(['school', 'government'] as $v)
                        <option value="{{ $v }}" @if($v == (old('type', $rec['type'] ?? ''))) selected @endif>
                            {{ $v }}
                        </option>
                    @endforeach
                </select>
                @error('type')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group col-md-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Semester
                    </span>
                </div>
                <select class="custom-select {{($readonly ?? false) ? 'disabled' : ''}} @error('semester') is-invalid @enderror {{($readonly ?? false) ? 'readonly' : ''}}"
                    id="semester" name="semester" required>
                    @foreach(['autumn', 'spring'] as $v)
                        <option value="{{ $v }}" @if($v == (old('semester', $rec['semester'] ?? ''))) selected @endif>
                            {{ $v }}
                        </option>
                    @endforeach
                </select>
                @error('semester')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

        </div>
        <div class="form-group">
            @include('includes.input-group', ['name' => 'name', 'label'=>'Exam title', 'value'=>old('name', $rec['title'] ?? ''), 'opts'=>['readonly'=> ($readonly ?? false)]])
        </div>
        <div class="form-group">
            <hr>
        </div>
        <div class="form-group">
            <label for="description">Description (optional)</label>
            <textarea name="description" {{($readonly ?? false) ? 'readonly' : ''}} class="form-control @error('description') is-invalid @enderror" id="description" rows="4">{{ old('description', $rec['description'] ?? '') }}</textarea>
        </div>
    </div>
    <div class="card-footer">
        @if(!$readonly)
            <button type="submit" class="btn btn-primary col-sm-4 float-right">Save</button>
        @endif
        <a href="{{ route('exams.index') }}" class="btn btn-default col-sm-2 float-right">Back</a>
    </div>
</form>

@push('css')
    <style>
		.input-group-text {
			min-width : 110px;
		}
    </style>
@endpush
