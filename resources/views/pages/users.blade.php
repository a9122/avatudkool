@extends('adminlte::page')

@section('title', ' | Profile')

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Users</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active">Users </li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card card-warning card-outline">

			<div class="card-body pb-0">
				@if ($users->links())
					<div class="float-right">
						{{ $users->links() }}
					</div>
				@endif
				<div class="row">
					@if (count($users))
						@foreach($users as $user)
							<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
								<div class="card bg-light d-flex flex-fill">
									<div class="card-header text-muted border-bottom-0"
										style="padding-left: 1rem;padding-right: 1rem;">

									</div>
									<div class="card-body pt-0" style="padding: 1rem;">
										<div class="row">
											<div class="col-12">
												<a href="{{$user->url}}" class="lead"><b>{{ $user->name }}</b></a>
											</div>
										</div>
										<div class="row mt-1">
											<div class="col-sm-12">
												<p class="text-muted text-sm mb-0">
													{!!  nl2br($user->description)  !!}</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@endif
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				@if ($users->links())
					<h5 class="float-left">Showing {{$users->firstItem()}} to {{$users->lastItem()}} of {{$users->total()}} entries
					</h5>
					<div class="float-right">
						{{ $users->links() }}
					</div>
				@endif
			</div>
			<!-- /.card-footer -->
		</div>
		<!-- /.card -->

	</section>
@stop