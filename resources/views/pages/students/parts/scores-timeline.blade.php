<?php

?>
<div class="timeline timeline-inverse">
    @foreach($scores as $grade_name => $results)
        <div class="time-label">
            <span class="bg-gradient-{{$loop->index === 0 ? 'primary':'secondary'}}">
				{{ $grade_name }}
                <small> {{ $loop->index === 0 ? ' (current year)' : '('.$loop->index.' year'.($loop->index > 1 ? 's': '').' ago)' }}</small>
            </span>
        </div>
        <div class="">
            <div class="timeline-item" style="margin-right: 0;margin-left:50px;">
                <h3 class="timeline-header text-right">{{ $rec->grade->name }}&nbsp;
                </h3>
                <table class="table table-bordered table-hover text-nowrap table-sm border-transparent" style="font-size:14px">
                    <thead>
                    <tr>

                        @if($results->count())
                            @foreach($results as $res)
                                <th class="text-center" style="width:120px">
                                    {{ $res->exam->subject->name }}
                                    <br>
                                    <small>{{ $res->exam->type }}, {{ $res->exam->semester }}</small>
                                </th>
                            @endforeach
                        @else
                            <th class="transparent" style="width:84%"></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    <tr>

                        @if($results->count())
                            @foreach($results as $res)
                                <td class="text-center" style="color:#fff;background:{{$res->color}}">
                                    {{ $res->score }}
                                </td>
                            @endforeach
                        @else
                            <td>
                                <span class="text-danger">Nothing found...</span>
                            </td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
@endforeach