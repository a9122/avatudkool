@extends('adminlte::page')

@section('title', ' | Add student')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Student "{{ $rec->full_name }}"</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('students.index') }}">Students</a></li>
                        <li class="breadcrumb-item active">edit {{ $rec->full_name }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <form class="form-horizontal" method="POST" action="{{ route('students.update', ['student'=>$rec->id]) }}">
                                @csrf
                                @method('PUT')
                                <div class="form-group row">
                                    <div class="input-group col-md-6">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                First name <code class="text-danger ml-1">*</code>
                                            </span>
                                        </div>
                                        <input class="form-control @error('first_name') is-invalid @enderror"
                                            type="text" id="first_name" name="first_name"
                                            minlength="2"
                                            value="{{ old('first_name', $rec->first_name) }}"
                                            placeholder="First name" required>
                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="input-group col-md-6">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Last name <code class="text-danger ml-1">*</code>
                                            </span>
                                        </div>
                                        <input class="form-control @error('last_name') is-invalid @enderror"
                                            type="text" id="last_name" name="last_name"
                                            minlength="2"
                                            value="{{ old('last_name', $rec->last_name) }}"
                                            placeholder="Last name" required>
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="isikukood" class="col-sm-2 col-form-label">Isikukood
                                        <code class="text-danger ml-1">*</code></label>
                                    <div class="col-sm-4">
                                        <input class="form-control @error('isikukood') is-invalid @enderror"
                                            type="text"  name="isikukood" id="isikukood-value"
                                            value="{{ old('isikukood', $rec->isikukood) }}" minlength="11"
                                            pattern="[\d\+\s]*" required
                                            placeholder="Isikukood code (11 digits)">

                                        @error('isikukood')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6" id="isikukood-validate">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lang" class="col-sm-2 col-form-label">Mother tongue <code class="text-danger ml-1">*</code></label>
                                    <div class="col-sm-10">
                                        <select required name="lang" id="lang"
                                            class="custom-select @error('lang') is-invalid @enderror">
                                            <option>- select lang. -</option>
                                            <option value="rus" @if(old('lang', $rec->lang) === 'rus') selected @endif>
                                                Russian
                                            </option>
                                            <option value="est" @if(old('lang', $rec->lang) === 'est') selected @endif>
                                                Estonian
                                            </option>
                                        </select>
                                        @error('lang')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="grade_id" class="col-sm-2 col-form-label">Current grade
                                        <code class="text-danger ml-1">*</code></label>
                                    <div class="col-sm-10">
                                        <select required name="grade_id" id="grade_id" required
                                            class="select2 @error('grade_id') is-invalid @enderror"
                                        >
                                            <option>- select grade -</option>
                                            @foreach($grades as $grade)
                                                <option value="{{ $grade->id }}"
                                                    {{ (old('grade_id', $rec->grade_id) == $grade->id) ? 'selected':'' }}
                                                    data-level="{{ $grade->level }}"
                                                    data-adv="{{$grade->students_count}} students">
                                                    {{ $grade->name }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('grade_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" {{($readonly ?? false) ? 'readonly' : ''}} class="form-control @error('description') is-invalid @enderror" id="description" rows="4">{{ old('description', $rec->description ?? '') }}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-7 col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{ route('students.index') }}"
                                            class="btn btn-default btn-block">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('css')
    <style>
		#filters .custom-select-sm,
		#filters, .select2-results {
			font-size : 12px !important;
		}
		label .label-descr {
			letter-spacing : -0.3px;
		}
		textarea#domain-list {
			resize : none;
		}
		#isikukood-validate span {
            line-height: 2.6em;
		}
    </style>
@endsection
@section('plugins.Select2', true)
@push('js')
    <script src="{{asset('vendor/isikukood.min.js')}}"></script>
    <script>
		$(function () {
            $('#isikukood-value').on('input', function(e){
                var _result_wrapper = $('#isikukood-validate');
                _result_wrapper.hide();
            });
            $('#isikukood-value').on('change', function(e){
                var _result_wrapper = $('#isikukood-validate');
                var _ik_code = $(this).val();
                _result_wrapper.show();
                if (_ik_code.length < 11){
                    _result_wrapper.html('<span class="text-warning"><i class="fas fa-exclamation-triangle"></i> Isikukood not valid. </span>');
                    return;
                } 
                var ik = new Isikukood(_ik_code);
                var is_valid = ik.validate();
                if (!is_valid){
                    _result_wrapper.html('<span class="text-warning"><i class="fas fa-exclamation-triangle"></i> Isikukood not valid. </span>');
                    return;
                }
                _result_wrapper.html('<span class="text-success"><i class="fas fa-check"></i>&emsp;' +
                    '<b>Birthday: '+ik.getBirthday().toLocaleDateString()+'; Gender: '+ik.getGender()+'</b></span>');
                
            });
            
            setTimeout(function(){
                $('#isikukood-value').trigger('change');
            }, 800);
            
			$('.select2').select2({
				theme: "bootstrap4",
				width: "style",
				placeholder: '- any -',
				allowClear: true,
				escapeMarkup: function (markup) {
					return markup;
				},
				templateResult: function (data) {
					var _custom = $(data.element).data('adv');
					if (!!_custom) {
						return $('<span>' + data.text + '</span><b style="float:right">' + _custom + '</b>');
					}
					return data.text;
				},
				templateSelection: function (data) {
					var _custom = $(data.element).data('adv');
					if (!!_custom) {
						return $('<span>' + data.text + '</span><b style="float:right">' + _custom + '</b>');
					}
					return data.text;
				}
			});
			bsCustomFileInput.init();
		});
    </script>
@endpush