@extends('adminlte::page')

@section('title', 'Student profile')

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Student Profile</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ route('students.index') }}">Students</a></li>
						<li class="breadcrumb-item active">Profile</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<!-- Profile  -->
					<div class="card card-primary card-outline">
						<div class="card-body box-profile">
							<div class="text-center">
								<img class="profile-user-img img-fluid img-circle"
									src="{{ $rec->image() }}"
									alt="User profile picture">
							</div>

							<h2 class="profile-username text-center">
								{{ $rec->full_name }}<br>
								<small style="font-size: 14px; color:#777;" title="Current grade">
									{{ $rec->grade->name }}
								</small>
							</h2>

							<ul class="list-group list-group-unbordered mb-3">
								<li class="list-group-item">
									<b>Isikukood: </b>
									<span class="text-primary float-right">{{ $rec->isikukood }}</span>
								</li>
								@if($rec->getPersonalCode()->validate())
									<li class="list-group-item">
										<b>Birth Date: </b>
										<span class="text-primary float-right">{{ $rec->birth_date }}</span><br>
										<small class="mt-1 text-dark float-right">{{ $rec->age }} years old</small>
									</li>
								@endif
								<li class="list-group-item">
									<b>Lang: </b>
									<span class="text-primary float-right">{{ ucfirst($rec->language) }}</span>
								</li>
								@if($rec->description)
									<li class="list-group-item border-0">
										<b class="mb-2">Description: </b>
										<p class="text-gray-dark m-0 mt-2 p-2 bg-light"
											style="min-height:4em; max-height: 8em; overflow-y: auto;">{!! nl2br($rec->description)  !!}</p>
									</li>
								@endif
							</ul>
							@if(current_user()->is_admin())
								<a href="{{ route('students.edit', ['student'=>$rec->id]) }}"
									class="btn btn-primary btn-block"><b>Edit</b></a>
							@endif
						</div>
						<!-- /.card-body -->
					</div>
				</div>
				<!-- /.col -->
				<div class="col-md-9">
					<div class="card">

						<div class="card-body">
						@include('pages.students.parts.scores-timeline', ['rec' => $rec, 'scores' => $scores])
						<!-- /.tab-content -->
						</div><!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
@stop