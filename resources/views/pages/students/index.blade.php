@extends('adminlte::page')

@section('title', ' | Subjects')

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card card-warning card-outline">
				<div class="card-header d-flex">
					<h3 class="card-title p-1">
						<i class="fas fa-users"></i> Students</h3>
				</div>
				<div class="card-body">
					@if(current_user()->is_admin())
						<a href="{{ route('students.create') }}" class="btn btn-primary mb-3">+ create new</a>
					@endif

					@if ($list->links())
						<div class="float-right">
							{{ $list->links() }}
						</div>
					@endif
					<div class="table-responsive">
						<table id="donors" class="table table-bordered table-hover text-nowrap" style="font-size:12px">
							<thead>
							<tr>
								<th style="width: 30px; text-align: center">#</th>
								<th class="text-primary">Student</th>
								<th>Isikukood</th>
								<th>Lang</th>
								<th>Grade</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							@if (count($list))
								@foreach($list as $rec)
									<tr>
										<td class="bg-lightblue text-center">
											{{ ($list->firstItem() + $loop->index) }}
										</td>
										<td style="white-space: break-spaces;">{{ $rec['full_name'] }}</td>
										<td class="tbl-col-project">
											<small>{!! nl2br($rec['isikukood']) !!}</small>
										</td>
										<td style="white-space: break-spaces;">{{ $rec['language'] }}</td>
										<td style="white-space: break-spaces;">{{ $rec['curr_grade'] }}</td>
										<td class="text-center" style="width:13%;">
											<a href="{{ route('students.show', ['student' => $rec->id]) }}" class="btn btn-primary btn-sm float-left mr-1">
												<i class="fas fa-eye"></i> </a>
											@if(current_user()->is_admin())
												<a href="{{ route('students.edit', ['student' => $rec->id]) }}" class="btn btn-info btn-sm float-left mr-1">
													<i class="fas fa-pencil-alt"></i> </a>
												<form action="{{ route('students.destroy', ['student' => $rec->id]) }}" method="post" class="float-left">
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Confirm delete student?')">
														<i class="fas fa-trash-alt"></i>
													</button>
												</form>
											@endif
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="4">
										<p class="text-gray text-lg">nothing found...</p>
									</td>
								</tr>
							@endif

							</tbody>
						</table>
					</div>
					@if ($list->links())
						<h4 class="float-left">Showing {{$list->firstItem()}} to {{$list->lastItem()}} of {{$list->total()}} entries </h4>
						<div class="float-right">
							{{ $list->links() }}
						</div>
					@endif
				</div>
				<!-- /.card-body -->
			</div>
		</div>
	</div>
@stop