@if ($dataset) {{-- [grade_name][[grade_id..][student_id..][exam_id..]]--}}
@foreach($dataset as $grade_name => $data)
    <hr style="opacity: .5">
    <div class="d-flex result-info">
        <p class="d-flex flex-column">
            <span class="text-bold text-lg grade">{{ $grade_name }}</span>
            <span class="type">{{ $type_label }}</span>
        </p>
        <p class="ml-auto d-flex flex-column text-right">
            <span class="text-primary">
                <i class="fas fa-calendar"></i>&nbsp;<span class="term">{{ $term_label ?? 'Last year' }}</span>
            </span>
            <b class="text-dark subject">{{ $subject['name'] ?? 'All subjects' }}</span>
        </p>
    </div>
    <?php
        $total_count = 0;
        $all_exams = [];
    ?>
    <div class="table-responsive">
        <table class="table table-bordered table-hover text-nowrap grade-scores table-sm" style="font-size:14px">
            <thead>
                <tr>
                    <th class="transparent" style="width:20%;" rowspan="2"></th>
                    @foreach($data as $grade_id => $grade_scores)
                        <?php
                        $exams = $grade_scores['exams'];
                        $exam_subjects = [];
                        $prev_subj_id  = null;
                        /** @var \App\Models\Exam $exam */
                        foreach($exams ?? [] as $exam):
                            $total_count++;
                            $all_exams[$exam->id] = ['grade_id' => $grade_id, 'scores' => $grade_scores['scores']];
                            $exam_subjects[$exam->subject_id] = $exam_subjects[$exam->subject_id]
                                ?? ['colspan' => 1, 'text' => $exam->subject->name];
                            if ($prev_subj_id == $exam->subject_id) {
                                $exam_subjects[$exam->subject_id]['colspan']++;
                            }
                            $prev_subj_id = $exam->subject_id;
                        endforeach;
                        ?>
                        @foreach($exam_subjects as $subj_id => $subj_data)
                            <th class="text-center bg-gradient-gray p-1" colspan="{{ $subj_data['colspan'] }}">
                                {{ $all_grades[$grade_id]['name'] }}
                            </th>
                        @endforeach
                    @endforeach
                </tr>
                <tr>
                    <th class="transparent hidden" style="width:20%;"></th>
                    @foreach($data as $grade_id => $grade_scores)
                        <?php
                        $exams = $grade_scores['exams'];
                        ?>
                        @foreach($exams as $exam)
                            <th class="text-center bg-gray-light p-1"
                                style="width:{{ (int)(80 / $total_count) }}%">
                                <span class="font-weight-normal text-sm">{{ $exam->type }}, {{ $exam->semester }}</span>
                            </th>
                        @endforeach
                    @endforeach
                </tr>
            </thead>
            <tbody>
            <?php
                $grade_scores = array_values($data)[0];
                $students_col_ready = true;
                $students = $grade_scores['students'] ?? [];
                $exams    = $grade_scores['scores'] ?? [];
            ?>
            @if (count($students))
                @foreach($students as $student)
                    <tr>
                        <td class="bg-gradient-lightblue p-2" style="white-space: nowrap;">
                            {{ $student->full_name }}
                        </td>
                        @foreach($all_exams as $exam_id => $rec)
                            @php
                                $score = $rec['scores'][$student->id]['scores'][$exam_id] ?? null;
                                $summary[$exam_id] = $summary[$exam_id]?? array_combine(array_merge(['total'], App\Models\Score::COLORS), [0, 0, 0, 0]);
                                if ($score){
                                    $summary[$exam_id][$score->color]++;
                                }
                                $summary[$exam_id]['total']++;
                            @endphp
                            @if (isset($score))
                                <td class="text-center" style="color:#fff;background:{{$score->color}}">
                                    @include('includes.score-value',
                                        ['student_id'=>$student->id, 'score_id'=>$score->id, 'exam_id'=>$exam_id, 'grade_id'=>$grade_id, 'score_value'=>$score->score])
                                </td>
                            @else
                                <td>
                                    @include('includes.score-value',['student_id'=>$student->id,'exam_id'=>$exam_id, 'grade_id'=>$grade_id])
                                </td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="{{count($all_exams) + 1}}">
                        <p class="text-gray text-lg">Nothing found...</p>
                    </td>
                </tr>
            @endif
            </tbody>                
                @if(($show_summary ?? true) && $summary)
                    <tfoot>
                    <tr>
                        <td class="transparent" style="padding: 4px 0;"></td>
                        @foreach($all_exams as $exam_id => $rec)
                            <td style="padding: 4px 0;">
                                <div class="position-relative mb-1 chart-block"
                                    data-c_red="{{$summary[$exam_id][App\Models\Score::COLOR_RED] ?? 0}}"
                                    data-c_green="{{$summary[$exam_id][App\Models\Score::COLOR_GREEN] ?? 0}}"
                                    data-c_yellow="{{$summary[$exam_id][App\Models\Score::COLOR_YELLOW] ?? 0}}"

                                    data-p_red="{{($summary[$exam_id][App\Models\Score::COLOR_RED] ?? 0)}}"
                                    data-p_green="{{($summary[$exam_id][App\Models\Score::COLOR_GREEN] ?? 0)}}"
                                    data-p_yellow="{{($summary[$exam_id][App\Models\Score::COLOR_YELLOW] ?? 0)}}"

                                    data-total="{{($summary[$exam_id]['total'] ?? 0)}}">
                                    <canvas class="summary-chart" width="140" height="140"></canvas>
                                    <div class="summary-legend p-1 m-auto mt-2"></div>
                                </div>
                            </td>
                        @endforeach
                    </tr>
                    </tfoot>
                @endif
        </table>
    </div>
@endforeach
@endif
