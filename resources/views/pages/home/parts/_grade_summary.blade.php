@if ($scores)
    <hr>
    <div class="d-flex result-info">
        <p class="d-flex flex-column">
            <span class="text-bold text-lg student">{{ $student->full_name }}</span>
            <span class="type">{{ $type_label }}</span>
        </p>
        <p class="ml-auto d-flex flex-column text-right">
            <span class="text-primary">
                <i class="fas fa-calendar"></i>&nbsp;<span class="term">{{ $term_label ?? 'Last year' }}</span>
            </span>
            <span class="text-muted subject">{{ $subject['name'] ?? 'All subjects' }}</span>
        </p>
    </div>
    @foreach($scores as $grade_name => $results)
        <div class="">
            <table class="table table-bordered table-hover text-nowrap table-sm border-transparent" style="font-size:14px">
                <thead>
                <tr>
                    <th class="transparent" style="width:15%;"></th>
                    @if($results->count())
                        @foreach($results as $res)
                            <th class="text-center" style="width:120px">
                                {{ $res->exam->subject->name }}
                                <br>
                                <small>{{ $res->exam->type }}, {{ $res->exam->semester }}</small>
                            </th>
                        @endforeach
                    @else
                        <th class="transparent" style="width:84%"></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-primary">{{ $grade_name }}</td>
                    @if($results->count())
                        @foreach($results as $res)
                            <td class="text-center" style="color:#fff;background:{{$res->color}}">
                                {{ $res->score }}
                            </td>
                        @endforeach
                    @else
                        <td><span class="lead">Nothing found...</span></td>
                    @endif
                </tr>
                </tbody>
            </table>
        </div>
    @endforeach
@else
    <p class="lead">Nothing found...</p>
@endif
