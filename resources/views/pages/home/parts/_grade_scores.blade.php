@if ($dataset) {{-- [grade_name][[grade_id..][student_id..][exam_id..]]--}}
@foreach($dataset as $grade_name => $data)
    <hr style="opacity: .5">
    <div class="d-flex result-info">
        <p class="d-flex flex-column">
            <span class="text-bold text-lg grade">{{ $grade_name }}</span>
            <span class="type">{{ $type_label }}</span>
        </p>
        <p class="ml-auto d-flex flex-column text-right">
            <span class="text-primary">
                <i class="fas fa-calendar"></i>&nbsp;<span class="term">{{ $term_label ?? 'Last year' }}</span>
            </span>
            <span class="text-muted subject">{{ $subject['name'] ?? 'All subjects' }}</span>
        </p>
    </div>

        @foreach($data as $grade_id => $grade_scores)
            {{-- $grade_scores['exams'], $grade_scores['students'], $grade_scores['scores']--}}
            @include('includes.table.grade-scores', ['grade_scores'=>$grade_scores, 'grade_id'=>$grade_id, 'subject'=>$subject])
        @endforeach
    @endif
@endforeach
@endif
