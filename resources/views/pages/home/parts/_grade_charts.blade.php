<?php

?>
<div class="row">
	<div class="col-12">
		<div class="card card-primary card-outline collapsed-card">
			<div class="card-header">
				<h3 class="card-title">
					<i class="fas fa-chart-bar"></i> Class by subject reports </h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-plus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
				<div class="row">

					<div class="col-md-6">
						<div class="tab-content">
							<div class="tab-pane active" id="details">
								<div class="d-flex">
									<p class="d-flex flex-column">
										<span class="text-bold text-lg">Math, 3A</span>
										<span> school & government</span>
									</p>
									<p class="ml-auto d-flex flex-column text-right">
										<span class="text-success">
											<i class="fas fa-arrow-up"></i> 13.1%
										</span>
										<span class="text-muted">Since last year</span>
									</p>
								</div>
								<!-- /.d-flex -->

								<div class="position-relative mb-4">
									<canvas id="details-chart" height="260"></canvas>
								</div>

								<div class="d-flex flex-row justify-content-end">
									<span class="mr-2">
										{{--<i class="fas fa-square text-primary"></i> This year--}}
									</span>

									<span>
										{{--<i class="fas fa-square text-gray"></i> Last year--}}
									</span>
								</div>
							</div>

							<div class="tab-pane" id="summary">
								<div class="d-flex">
									<p class="d-flex flex-column">
										<span class="text-bold text-lg">Mother tongue, 3B</span>
										<span> school only</span>
									</p>
								</div>
								<!-- /.d-flex -->

								<div class="position-relative mb-4">
									<canvas id="summary-chart" height="160"></canvas>
								</div>
							</div>
						</div>
						<ul class="nav nav-pills">
							<li class="nav-item">
								<a class="nav-link active" href="#details" data-toggle="tab">Details</a></li>
							<li class="nav-item">
								<a class="nav-link" href="#summary" data-toggle="tab">Summary</a>
							</li>

						</ul>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Select class</label>
							<select name="grade_id" class="custom-select select2" data-placeholder="- select class -">
								<option value="-1">- all -</option>
								@foreach($grades as $grade)
									<option value="{{ $grade->id }}"> {{ $grade->name }} </option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Select subject</label>
							<select name="subject_id" class="custom-select select2" data-placeholder="- select subject -">
								<option></option>
								@foreach($subjects as $subject)
									<option value="{{ $subject->id }}"> {{ $subject->name }} </option>
								@endforeach
							</select>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input class="custom-control-input" type="checkbox" id="customCheckbox1" checked="">
										<label for="customCheckbox2" class="custom-control-label"> school exam </label>
									</div>
								</div>

							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input class="custom-control-input" type="checkbox" id="customCheckbox2">
										<label for="customCheckbox2" class="custom-control-label"> government exam </label>
									</div>
								</div>

							</div>

						</div>
						<button type="submit" class="btn btn-primary">Show</button>
					</div>
				</div>
			</div>
			<div class="card-footer">

			</div>
		</div>
	</div>
</div>