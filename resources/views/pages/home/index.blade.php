@extends('adminlte::page')

@section('title', ' | Students scores')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            {{-- !!! summary info !!! --}}
            <div id="stats-info">
                <div id="stats-info-content" class="row">
                    <div class="col-md-3">
                        <!-- small box -->
                        <div class="small-box bg-gradient-purple ">
                            <div class="inner">
                                <h3>{{ $stats['students'] }}</h3>
                                <p>Students</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{url('/students')}}" class="small-box-footer"> More info
                                <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-md-3">
                        <!-- small box -->
                        <div class="small-box bg-gradient-primary">
                            <div class="inner">
                                <h3>{{$stats['grades']}}</h3>
                                <p>Grades</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-chalkboard-teacher"></i>
                            </div>
                            <a href="{{ url('/grades') }}" class="small-box-footer"> More info
                                <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-md-3">
                        <!-- small box -->
                        <div class="small-box bg-gradient-success">
                            <div class="inner">
                                <h3>{{ $stats['subjects'] }}</h3>

                                <p>Subjects</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-book-open"></i>
                            </div>
                            <a href="{{ url('/subjects') }}" class="small-box-footer">More info
                                <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-md-3">
                        <!-- small box -->
                        <div class="small-box bg-gradient-warning">
                            <div class="inner">
                                <h3>{{ $stats['exams'] }}</h3>

                                <p>Exams</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-boxes"></i>
                            </div>
                            <a href="{{ url('/exams') }}" class="small-box-footer">More info
                                <i class="fas fa-arrow-circle-right"></i></a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-purple card-outline">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-chart-bar"></i>&nbsp;Student scores</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 form-block">
                                    <form class="ajax-form" id="student-scores" action="{{ route('home.student-scores') }}">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Student</label>
                                                    <select name="student_id" class="select2" required data-placeholder="- select student -">
                                                        <option></option>
                                                        @foreach($students as $student)
                                                            <option value="{{ $student->id }}"
                                                                data-level="{{ $student->grade->level }}"
                                                                data-adv="{{$student->grade->name}}">
                                                                {{ $student->full_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>
                                                        Select
                                                        <span class="text-green">subject</span>
                                                    </label>
                                                    <select name="subject_id" class="custom-select select2" data-placeholder="- select subject -">
                                                        <option value="-1">- all -</option>
                                                        @foreach($subjects as $subject)
                                                            <option value="{{ $subject->id }}">
                                                                {{ $subject->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                @include('includes.search-period-select', ['pref'=>'st-'])
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group input-group-sm">
                                                    <label>Exam type</label>
                                                    <select name="exam_type" class="custom-select">
                                                        <option value="0">school and government</option>
                                                        <option value="{{\App\Models\Exam::TYPE_SCHOOL}}">
                                                            school only
                                                        </option>
                                                        <option value="{{\App\Models\Exam::TYPE_GOVERNMENT}}">
                                                            government only
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <label>&nbsp;</label>
                                                <button type="submit" class="btn btn-primary btn-sm btn-block disabled">Show</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12 result-block">
                                    <p class="lead"> Select student and click "Show" </p>
                                    {{-- filtering result show here ... --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">

                        </div>
                    </div>
                </div>
            </div>
            {{-- !!! report form 3 !!! --}}
            <div class="row">
                <div class="col-12">
                    <div class="card card-blue card-outline">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-chart-line"></i>&nbsp;Grade scores</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 form-block">
                                    <form class="ajax-form" id="grade-scores" action="{{ route('home.grade-scores') }}">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group multiple-float-none">
                                                    <label>Grade(-s)</label>
                                                    <select name="grade_id[]"
                                                        class="select2" multiple required
                                                        data-placeholder="- select one grade(-s) -">
                                                        @foreach($grades as $grade)
                                                            <option value="{{ $grade->id }}"
                                                                data-level="{{ $grade->level }}"
                                                                data-adv="{{$grade->students_count}} students">
                                                                {{ $grade->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Select
                                                        <span class="text-green">subject</span>
                                                    </label>
                                                    <select name="subject_id" class="select2" data-placeholder="- select subject -">
                                                        <option value="-1">- all -</option>
                                                        @foreach($subjects as $subject)
                                                            <option value="{{ $subject->id }}">
                                                                {{ $subject->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 term-selector-block disabled">
                                                <div class="form-group input-group-sm">
                                                    <label>Show result by</label>
                                                    <select name="term" class="custom-select">
                                                        <option value="0">Latest year</option>
                                                        @for($i=0;$i<9;$i++)
                                                            <option value="{{ $i+1 }}">Last {{ $i+2 }} years</option>
                                                        @endfor
                                                        <option value="all">All time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group input-group-sm">
                                                    <label>Exam type</label>
                                                    <select name="exam_type" class="custom-select">
                                                        <option value="0">school and government</option>
                                                        <option value="{{\App\Models\Exam::TYPE_SCHOOL}}">
                                                            school only
                                                        </option>
                                                        <option value="{{\App\Models\Exam::TYPE_GOVERNMENT}}">
                                                            government only
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <label>&nbsp;</label>
                                                <button type="submit" class="btn btn-primary btn-sm btn-block disabled">Show</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12 result-block">
                                    <p class="lead"> Select grade(-s), subject and click "Show" </p>
                                    {{-- filtering result show here ... --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('css')
    <style>
		#filters .custom-select-sm,
		#filters, .select2-results {
			font-size : 12px !important;
		}
		label .label-descr {
			letter-spacing : -0.3px;
		}
		textarea#domain-list {
			resize : none;
		}
		option:disabled {
			color   : silver;
			display : none;
		}
    </style>
@stop

@section('plugins.Select2', true)
@section('plugins.Datatables', true)
@section('plugins.DatatablesPlugins', true)
@section('plugins.Chartjs', true)

@push('js')
    <script>
		$(function () {
            /*ChartJS*/
			var tick = {
				fontColor: '#495057',
				fontStyle: 'bold'
			};
			$('.select2').select2({
				theme: "bootstrap4",
				width: "style",
				placeholder: '- any -',
				allowClear: true,
				escapeMarkup: function (markup) {
					return markup;
				},
				templateResult: function (data) {
					var _custom = $(data.element).data('adv');
					if (!!_custom) {
						return $('<span>' + data.text + '</span><b style="float:right">' + _custom + '</b>');
					}
					return data.text;
				},
				templateSelection: function (data) {
					var _custom = $(data.element).data('adv');
					if (!!_custom) {
						return $('<span>' + data.text + '</span><b style="float:right">' + _custom + '</b>');
					}
					return data.text;
				}
			});
			bsCustomFileInput.init();
			$('input.term-switch').on('change', function (e) {
				var _wrapper      = $(this).parents('.search-term');
				var _period_block = _wrapper.find('.term-selector.period');
				var _years_block  = _wrapper.find('.term-selector.years');
				var _sel_type     = $('.term-switch:checked').val();

				_wrapper.find('.term-selector').toggleClass('hidden');

				console.log('term-switch to ', _sel_type);
				if (_sel_type == 'years') {

					_period_block.find('.term').prop('disabled', true);
					_years_block.find('.term').removeAttr('disabled');
				} else {
					_period_block.find('.term').removeAttr('disabled');
					_years_block.find('.term').prop('disabled', true);
				}
			});

			$('#student-scores select[name="student_id"]').on('change', function (e) {
				if ($(this).val()) {
					var _selected     = $(this).find(":selected");
					var _selected_lvl = _selected.data('level');
					console.log("Selected level: ", _selected_lvl);

					if (!!_selected_lvl) {
						$(this).parents('form').find('.term option').filter(function () {
							const disabled = $(this).attr('value') > (_selected_lvl - 1);
							console.log($(this).attr('value') + (disabled ? 'disabled' : ''));
							return disabled;
						}).prop('disabled', true);
					}
					$(this).parents('form').find('.term-selector select').val(0);
					$(this).parents('form').find('button[type="submit"]').removeClass('disabled');
				} else {
					$(this).parents('form').find('.term option').prop('disabled', false);
					$(this).parents('form').find('button[type="submit"]').addClass('disabled');
				}
			});

			var _validate_grade_form = function (form, enabled) {
				if (!!enabled) {
					form.find('.term-selector-block').removeClass('disabled');
					form.find('button[type="submit"]').removeClass('disabled');
				} else {
					form.find('.term-selector-block').addClass('disabled');
					form.find('button[type="submit"]').addClass('disabled');
				}
			};
			$('#grade-scores select[name="grade_id[]"]').on('change select2:unselect', function (e) {
				var _selected = $(this).find(":selected");
				_validate_grade_form($(this).parents('form'), _selected.length);
                /* can multiselect grades only with same level */
				if (_selected.length) {
					var _selected_lvl = _selected.data('level');
					if (_selected_lvl) {
						$(this).find('option').prop('disabled', true);
						$(this).find('option[data-level="' + _selected_lvl + '"]').prop('disabled', false);
					}
				} else {
					$(this).find('option').prop('disabled', false);
				}
			});

			$('form.ajax-form').on('submit', function (e) {
				e.preventDefault();
				var _form         = $(this);
				var _result_block = $(this).parents('.card-body').find('.result-block');
				var _req_link     = $(this).attr('action');

				NProgress.start();
				_form.addClass('disabled');
				$.get(_req_link, _form.serialize(), function (data) {
					_result_block.html(data);

					var f__calc_percent    = function (data, key) {
						return Math.round((data[key] / data['total']) * 100.0);
					};
					var f__calc_trend      = function (prev, curr) {
						return Math.round((1 - (prev / curr)) * 100);
					};
					var f__get_text        = function (val, label) {
						return (val > 1) ? label + 's' : label;
					};
					var f__get_score_color = function (val) {
						var scoreColors = {
							red: '#F76954',
							yellow: '#FCCE14',
							green: '#00B65A'
						};
						return (val <= 50)
							? scoreColors.red
							: ((val <= 75) ? scoreColors.yellow : scoreColors.green);
					};

					if (_result_block.find('.chart-block').length) {
						_result_block.find('.chart-block').each(function () {
							var _block = $(this);

							var _canvas, _legend;
							if (_block.find('.bar-chart').length) {
								_canvas      = _block.find('.bar-chart');
								var _dataset = _block.data('dataset');
								var _labels  = [];
								var _cats    = [];

								var _bar_dataset    = {
									borderWidth: 0,
									backgroundColor: [],
									data: [],
									order: 2,
									strokeColor: "rgba(0,191,255,0.8)"
								};
								var _trend_dataset  = {
									data: [],
									"type": "line",
									order: 1,
									borderColor: "#777",
									borderWidth: 1,
									"fill": false,
									borderJoinStyle: 'bevel'
								};
								var _prev_score_val = 0;

								for (let key in _dataset) {
									_cats.push(key);

									_dataset[key].forEach(function (a) {
										if (!a || !a.score) {
											return;
										}
										_bar_dataset.data.push(a.score);
										_trend_dataset.data.push(a.score);
										if (_prev_score_val > 0) {
											console.log(_prev_score_val);
											var trend = f__calc_trend(_prev_score_val, a.score);
											_labels.push([
												[key + ', ' + a.exam.type],
												[((trend > 0) ? '↑ ' : '↓ ') + trend + '%']
											]);
										} else {
											_labels.push(key + ', ' + a.exam.type);
										}
										_prev_score_val = a.score;
										_bar_dataset.backgroundColor.push(f__get_score_color(a.score || 0));
									});
								}
								console.log('Chart data: ', _bar_dataset, _trend_dataset, _labels);
								var _chart_bar = new Chart(_canvas, {
									type: 'bar',
									data: {
										labels: _labels,
										datasets: [_bar_dataset, _trend_dataset]
									},
									options: {
										layout: {
											padding: {
												left: 0,
												right: 0,
												top: 20,
												bottom: 20
											}
										},
										events: [],
										responsive: false,
										maintainAspectRatio: true,
										legend: {
											display: false
										},
										scales: {
											xAxes: [{
												gridLines: {
													display: false,
													offsetGridLines: true
												}
											}],
											yAxes: [{
												ticks: {
													beginAtZero: true
												},
												gridLines: {
													display: true
												}
											}]
										},
										animation: {
											duration: 1000,
											onComplete: function () {
												var chartInstance = this.chart,
												    ctx           = chartInstance.ctx;
												ctx.font          = Chart.helpers.fontString(
													12,
													Chart.defaults.global.defaultFontStyle,
													Chart.defaults.global.defaultFontFamily);
												ctx.textAlign     = 'center';
												ctx.fillStyle     = '#666';
												ctx.textBaseline  = 'bottom';
												this.data.datasets.forEach(function (dataset, i) {
													var meta = chartInstance.controller.getDatasetMeta(i);
													meta.data.forEach(function (bar, index) {
														if (dataset.data[index] > 0) {
															var data = dataset.data[index];
															ctx.fillText(data, bar._model.x, bar._model.y-5);
														}
													});
												});
											}
										}
									}
								});
							}

							if (_block.find('.summary-chart').length) {
								_canvas        = _block.find('.summary-chart');
								_legend        = _block.find('.summary-legend');
								var _chart_pie = new Chart(_canvas, {
									type: 'pie',
									data: {
										labels: [
											_block.data('p_red') +
											f__get_text(_block.data('p_red'), ' student') + ', ' +
											f__calc_percent(_block.data(), 'p_red') + '%',

											_block.data('p_yellow') +
											f__get_text(_block.data('p_yellow'), ' student') + ', ' +
											f__calc_percent(_block.data(), 'p_yellow') + '%',

											_block.data('p_green') +
											f__get_text(_block.data('p_green'), ' student') + ', ' +
											f__calc_percent(_block.data(), 'p_green') + '%'
										],
										datasets: [
											{
												data: [
													_block.data('c_red'),
													_block.data('p_yellow'),
													_block.data('c_green')
												],
												backgroundColor: ['#F76954', '#FCCE14', '#00B65A']
											}
										]
									},
									options: {
										tooltips: {
											// Disable the on-canvas tooltip
											enabled: true,

											callbacks: {
												label: function (tooltipItem, data) {
													console.log(tooltipItem, data);
													return ' ' + data.labels[tooltipItem.index] || '-';
												}
											}
										},
										responsive: false,
										legend: {
											//weight: 300,
											//position: 'bottom',
											//align: 'start',
											display: false
											//labels: {
											//	boxWidth: 10,
											//	padding: 6
											//}
										},
										legendCallback: function (chart) {
											console.log("???");
											var text   = [];
											var colors = ['#F76954', '#FCCE14', '#00B65A'];
											text.push('<ul class="' + chart.id + '-legend">');
											for (var i = 0; i < chart.data.labels.length; i++) {
												text.push('<li><span style="background-color:' +
												          colors[i] + '">&nbsp;</span>');
												const label = chart.data.labels[i];
												text.push(Array.isArray(label) ? label.join('<br>') : label);
												text.push('</li>');
											}
											text.push('</ul>');
											return text.join('');
										},
										maintainAspectRatio: true,
										animation: {
											animateRotate: true,
											animateScale: true
										}
									}
								});
								_legend.html(_chart_pie.generateLegend());
							}
						});
					}

					$("table.data-table").DataTable({
						"pageLength": 30,
						"responsive": false,
						"lengthChange": false,
						"autoWidth": false
					});
				}).always(function (data) {
					console.log('DONE:', data.status);
					if (data.statusText === 'error') {
						UserNotice.error('Plz, try again later...', 'Something went wrong');
					}
					_form.removeClass('disabled');
					NProgress.done();
				});
			});

		});
    </script>
@endpush