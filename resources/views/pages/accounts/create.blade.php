@extends('adminlte::page')

@section('title', 'LinksInspector | Add user')

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Create {{ ($force_teacher ?? false) ? 'teacher' : 'accaunt' }}</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item"><a href="/accounts">Accounts</a></li>
						<li class="breadcrumb-item active">New account</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">

					<!-- Profile Image -->
					<div class="card card-primary card-outline">
						<div class="card-body box-profile">
							<form class="form-horizontal" method="POST" action="{{ route('accounts.store') }}">
								@csrf
								@method('POST')
								<div class="form-group row">
									<label for="inputName" class="col-sm-2 col-form-label">Name</label>
									<div class="col-sm-10">
										<input type="text" class="form-control @error('name') is-invalid @enderror"
											id="inputName" name="name" value="{{ old('name') }}"
											placeholder="Name">

										@error('name')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
									<div class="col-sm-10">
										<input type="email" class="form-control @error('email') is-invalid @enderror"
											id="inputEmail" name="email" value="{{ old('email') }}"
											placeholder="Email">

										@error('email')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
								</div>
								{{-- Password field --}}
								<div class="form-group row">
									<label for="inputPass" class="col-sm-2 col-form-label">Password</label>
									<div class="col-sm-10">
										<div class="input-group">
											<input type="password"
												class="form-control @error('password') is-invalid @enderror"
												name="password" id="inputPass" value="{{ old('password') }}"
												placeholder="{{ __('adminlte::adminlte.password') }}"
												data-toggle="password">
											<div class="input-group-append">
												<div class="input-group-text password-show-toggle"
													title="Show / hide password">
													<span class="fas fa-eye"></span>
												</div>
											</div>

											@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>

									</div>
								</div>


								{{-- Confirm password field --}}
								<div class="form-group row">
									<label for="inputPass" class="col-sm-2 col-form-label">&nbsp;</label>
									<div class="col-sm-10">
										<div class="input-group">
											<input type="password" name="password_confirmation"
												class="form-control @error('password_confirmation') is-invalid @enderror"
												data-toggle="password"
												placeholder="{{ __('adminlte::adminlte.retype_password') }}">

											<div class="input-group-append">
												<div class="input-group-text password-show-toggle"
													title="Show / hide password">
													<span class="fas fa-eye"></span>
												</div>
											</div>
											@error('password_confirmation')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>


									</div>
								</div>
								<input type="hidden" name="type" value="teacher">
								<div class="form-group row {{ ($force_teacher ?? false) ? 'hidden' : '' }}">
									<div class="offset-sm-2 col-sm-10">
										<div class="checkbox">
											<label><input type="checkbox" name="type" value="admin"
													{{ old('type') === 'admin' ? 'checked' : '' }}>&nbsp;<b class="text-red">Is admin</b> account</label>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<div class="offset-sm-2 col-sm-10">
										<button type="submit" class="btn btn-flat btn-primary">Save account</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@stop