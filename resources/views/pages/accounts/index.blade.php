@extends('adminlte::page')

@section('title', ' | '.($title ?? 'Accounts'))

@section('content')
	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card card-warning card-outline">
			<div class="card-header d-flex">
				<h3 class="card-title p-1">
					<i class="fas fa-list-alt"></i> {{ ($title ?? 'Accounts') }}
				</h3>
				<ul class="nav nav-pills ml-auto">
					<li class="nav-item">
						<span class="nav-link">Search by name or email:</span>
					</li>
					<li class="nav-item" style="width: 240px;">
						<form method="get">
							<input type="search" class="form-control search-title form-control-sm nav-link" name="filter[q]" placeholder="Search by name or email" value="{{ $filter['q'] ?? '' }}">
						</form>
					</li>
				</ul>
			</div>
			<div class="card-body pb-0">
				@if(current_user()->is_admin())
					<a href="{{ isset($title) ? route('accounts.create', ['t'=>1]) : route('accounts.create') }}" class="btn btn-primary mb-3">+ create new</a>
				@endif
				@if ($list->links())
					<div class="float-right">
						{{ $list->links() }}
					</div>
				@endif
				<div class="row">
					@if (count($list))
						@foreach($list as $user)
							<div class="col-12 col-sm-4 col-md-3 d-flex align-items-stretch flex-column">
								<div class="card bg-light d-flex flex-fill">
									<div class="card-header text-muted border-bottom-0"
										style="padding-left: 1rem;padding-right: 1rem;">
										<span class="">
											@if($user->is_admin())
                                                <b class="text-danger">administrator</b>
											@else
												<span class="text-primary">teacher</span>
											@endif
										</span>
									</div>
									<div class="card-body pt-0" style="padding: 1rem;">
										<div class="row">
											<div class="col-7">
												<h2 class="lead"><b>{{ $user->name }}</b></h2>

												<ul class="ml-3 mb-0 fa-ul text-muted" style="font-size: 15px;">
													@if($user->email)
														<li class="small mb-1">
															<a href="mailto:{{$user->email}}" title="Send mail">
																<span class="fa-li"><i class="fas fa-envelope"></i>
																</span>
															</a>
															{{ $user->email }}
														</li>
													@endif
												</ul>
											</div>
											<div class="col-5 text-center">
												<img src="{{$user->adminlte_image()}}"
													alt="{{$user->name}} avatar" class="img-circle img-fluid">
											</div>
										</div>
									</div>
									<div class="card-footer">
										<div class="text-right">
											@if(current_user()->is_admin())
												<a href="{{route('accounts.show', ['account' => $user->id])}}" class="btn btn-sm btn-primary btn-flat mr-1">
													<i class="fas fa-user"></i> View profile
												</a>
												<form
													action="{{ route('accounts.destroy', ['account' => $user->id]) }}"
													method="post" class="float-right">
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-sm btn-flat btn-danger"
														onclick="return confirm('Confirm delete account?')">
														<i class="fas fa-trash-alt"></i>
													</button>
												</form>
											@endif
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@endif
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				@if ($list->links())
					<h4 class="float-left">Showing {{$list->firstItem()}} to {{$list->lastItem()}} of {{$list->total()}} entries
					</h4>
					<div class="float-right">
						{{ $list->links() }}
					</div>
				@endif
			</div>
			<!-- /.card-footer -->
		</div>
		<!-- /.card -->

	</section>
@stop