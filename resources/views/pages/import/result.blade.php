<hr>
<dl class="row">
    <dt class="col-sm-4 text-primary">Total lines:</dt>
    <dd class="col-sm-8 text-primary"><b>{{ $total_count ?? 0 }}</b></dd>

    <dt class="col-sm-4 text-success">Imported rows:</dt>
    <dd class="col-sm-8 text-success"><b>{{ $imported_count ?? 0 }}</b></dd>

    <dt class="col-sm-4 text-danger">Skipped rows:</dt>
    <dd class="col-sm-8 text-danger"><b>{{ $skipped_count ?? 0 }}</b></dd>

    @if(!empty($records_logs ?? []))
        <dd class="col-sm-8 offset-sm-4">
            <hr>
        </dd>
        <dt class="col-sm-4 text-danger">Imported data:</dt>
        <dd class="col-sm-8">
            <div class="listing">
                @foreach($records_logs as $type => $item_ids)
                    <details>
                        <summary class="text-green">
                            {{ count($item_ids ?? []) }} <b class="text-capitalize">{{ $type }}</b> records
                        </summary>
                        <div class="mt-2 ml-3">
                            @foreach($item_ids as $n => $item_id)
                                <p>{{ mb_substr($type, 0, -1) }} with #{{ $item_id }}</p>
                            @endforeach
                        </div>
                    </details>
                @endforeach
            </div>
        </dd>
    @endif
</dl>