@extends('adminlte::page')

@section('title', ' | Import students data')

@section('content')<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">
	<div class="card card-primary card-outline">
		<div class="card-header">
			<h3 class="card-title">
				<i class="fas fa-file-import"></i> Import students data from .CSV
			</h3>
		</div>
		<div class="card-body">
			<form class="form-horizontal" enctype="multipart/form-data"
				id="import-form" action="{{ route('import.process') }}" method="post">
				@csrf
				<div class="form-group row">
					<div class="custom-file col-sm-8">
						<input type="file" class="custom-file-input" name="file" accept=".csv" id="import-file" required>
						<label class="custom-file-label" for="import-file">Select .csv file</label>
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary btn-block">
							<i class="fas fa-file-import"></i> Import
						</button>
					</div>
				</div>
				<div class="row" id="import-options">
					<div class="col-md-6 mb-4">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input"
								name="options[overwrite]" id="overwrite" checked="" value="1">
							<label class="custom-control-label" for="overwrite">Overwrite existing records</label>
						</div>
					</div>
					<div class="col-md-6 mb-4">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input"
								name="options[validate_student_code]" id="validate_student_code" value="1">
							<label class="custom-control-label" for="validate_student_code">
								Check validity student isikukood
							</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" checked=""
								name="options[import_all]"  id="import-all" value="1">
							<label class="custom-control-label" for="import-all">Import All</label>
						</div>

					</div>
					<div class="col-md-12 mt-2 mb-2">
						<hr style="margin: 0;">
						<div id="import-parts" class="row mt-2 mb-2 disabled">
							<div class="col-md-4">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" checked="" id="import-subject" name="options[import][subject]" value="1">
									<label class="custom-control-label" for="import-subject">
										Import Subjects
									</label>
								</div>
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" checked="" id="import-grades" name="options[import][grades]" value="1">
									<label class="custom-control-label" for="import-grades">
										Import Grades
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input depend"
										data-depends="import-subject,import-grades"
										checked="" id="import-exams" name="options[import][exams]" value="1">
									<label class="custom-control-label" for="import-exams">Import Exams</label>
								</div>
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input depend"
										data-depends="import-grades"
										checked="" id="import-students" name="options[import][students]" value="1">
									<label class="custom-control-label" for="import-students">Import Students</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="row">
				<div class="col-md-12" id="import-info"></div>
			</div>
			<div class="row">
				<div class="col-md-12" id="import-result"></div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('css')
	<style>
		#filters .custom-select-sm,
		#filters, .select2-results {
			font-size : 12px !important;
		}
		label .label-descr {
			letter-spacing : -0.3px;
		}
		textarea#domain-list {
			resize : none;
		}
	</style>
@endsection
@push('js')
	<script src="{{asset('vendor/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
	<script src="{{asset('vendor/chart.js/Chart.js')}}"></script>
	<script src="{{asset('vendor/datatables/datatables.min.js') }}"></script>
	<script>
		$(function () {
			bsCustomFileInput.init();
			$('.select2').select2({
				theme: "bootstrap4",
				width: "style",
				placeholder: '- any -',
				allowClear: true
			});
			$('form #import-parts input[type="checkbox"]').on('change', function (e) {
				var _can_checked = true;
				console.log($(this));
				$('form #import-parts .depend').each(function () {
					var _elem = $(this);
					if (_elem.is(':checked') && _elem.data('depends')) {
						var _depends = _elem.data('depends').split(',');
						_depends.forEach(function (el_id) {
							if (!$('form #import-parts #'+el_id).is(':checked')){
								_elem.prop('checked', false);
								return false;
							}
						});
					}
				});
			});
			$('form #import-all').on('change', function (e) {
				console.log('-');
				$('form #import-parts').toggleClass('disabled');
				if ($(this).is(':checked')) {
					$('#import-parts input[type="checkbox"]').prop('checked', 1);
				}
			});
			$('form#import-form').on('submit', function (e) {
				e.preventDefault();
				var _form     = $(this);
				var file_data = _form.find('[name="file"]').prop('files')[0];
				var form_data = new FormData();
				form_data.append('file', file_data);
				form_data.append('_token', $('meta[name=csrf-token]').attr('content'));

				$.ajax({
					url: _form.attr('action'), // <-- point to server-side PHP script
					dataType: 'text',  // <-- what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					beforeSend: function () {
						NProgress.configure({trickle: true, showSpinner: true});
						NProgress.start();
					},
					success: function (resp) {
						$('#import-result').html(resp).slideDown('fast', function () {
							$('#import-result')[0].scrollIntoView();
							setTimeout(function () {

							});
						});
					}
				}).always(function () {
					NProgress.done();
					NProgress.configure({trickle: true, showSpinner: false});
					_form.find('[type="submit"]').removeClass('disabled');
				});
			});
		});
	</script>
@endpush;