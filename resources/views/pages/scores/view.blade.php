@extends('adminlte::page')

@section('title', 'LinksInspector | Task info')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-warning card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-tc"></i>
                        Task #{{$task->id}} info
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-left col-md-11">
                                <a href="{{ route('task.edit', ['task' => $task->id]) }}"
                                   class="btn btn-info btn-md btn-block">
                                    <i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Edit
                                </a>
                                
                                <a href="{{ route('task.execute', ['task' => $task->id]) }}"
                                   class="btn btn-success btn-md btn-block" onclick="return confirm('Confirm task execution')">
                                    <i class="fas fa-play"></i>&nbsp;&nbsp;Execute once
                                </a>

                                <form
                                    action="{{ route('task.destroy', ['task' => $task->id]) }}" method="post" class="btn-block">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-md btn-block"
                                            onclick="return confirm('Confirm the deletion')">
                                        <i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Delete
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-4 text-gray ">Target: </div><div class="col-md-8 text-primary">{{ $task->link }}</div>
                                <div class="col-md-4 text-gray">Period: </div><div class="col-md-8 text-primary" style="margin-top:2px;">{{ $task->period_title }}</div>
                                <div class="col-md-4 text-gray">Status: </div><div class="col-md-8 text-primary" style="text-transform: capitalize;">{{ $task->status }}</div>
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-4 text-gray">Domains:</div> <div class="col-md-8 text-primary">{!! nl2br($task->anchors) !!}</div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 left-panel">
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title">Inspections log</h3>
                </div>
          <!-- /.card-header -->
                <div class="card-body">
                    @if ($task_execs->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover text-nowrap table-striped" id="task-execs">
                            <thead>
                            <tr>
                                <th style="width: 30px">#</th>
                                <th>Inspection date</th>
                                <th>Status</th>
                                <th>Found links</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($task_execs as $exec)
                                <tr>
                                    <td>{{ $exec->id }}</td>
                                    <td>{{ $exec->created_at }}</td>
                                    <td style="text-transform: capitalize;">{{ $exec->status }}</td>
                                    <td>{{ $exec->logs->count() }}</td>
                                    <td class="text-center" >
                                        @if($exec->logs->count())
                                        <a data-show-log="exec-log-{{$exec->id}}"
                                            data-link="{{route('task.execution.view', ['task'=>$task->id, 'execution'=>$exec->id])}}"
                                           class="btn btn-primary btn-sm float-left mr-1">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                              <tr>
                                <th style="width: 30px">#</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Found links</th>
                                <th>Actions</th>
                              </tr>
                            </tfoot>
                        </table>
                    </div>
                        {!! $task_execs->onEachSide(5)->links() !!}
                    @else
                        <p>Executions not found...</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-12 right-panel" style="display:none;">
            <div class="card card-info card-outline">
                <div class="card-header border-0">
                </div>
                <div class="card-body" id="task-execution-logs">
                    {{-- ... logs loading with ajax ... --}}
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
  <link rel="stylesheet" href="{{asset('vendor/datatables/datatables.min.css')}}">
  <style>
    .table-hover tbody tr.active,.table-hover tbody tr.selected{background:#8bc2e9;}
  </style>
@stop
@section('js')
<script src="{{asset('vendor/datatables/datatables.min.js') }}"></script>
<script>
  $(function () {
    $("#task-execs").DataTable({
        "responsive": true,
        "paging": false,
        "searching": false,
	    "ordering": false,
	    "info": false,
        "lengthChange": false,
        "autoWidth": false
	});
    
    $('a[data-show-log]').on('click', function(e){
		var _tr = $(this).parents('tr');
        var _log_id = $(this).data('show-log');
        var _req_link = $(this).data('link');

        if (!!$('.right-panel:visible').length) {
		    $('.right-panel:visible').hide();
	    }
	    _tr.addClass('active').siblings('tr').removeClass('active');

	    $.get(_req_link, function(data){
			$('#task-execution-logs').html(data);

	        $("table.exec-log-dt").DataTable({
		        "responsive": true, "lengthChange": false, "autoWidth": false
	        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

			$('.right-panel').show();
	        $('.right-panel #' + _log_id).show();
	        $('.right-panel #' + _log_id)[0].scrollIntoView();
        });
    });
  });
</script>
@stop
