@extends('adminlte::page')

@section('title', ' | students exams scores')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-warning card-outline">
                <div class="card-header d-flex">
                    <h3 class="card-title p-1">
                        <i class="fas fa-users"></i>
                        Students exams scores
                    </h3>
                </div>
                <div class="card-body">
					@if(current_user()->is_admin())
						<a href="{{ route('scores.create') }}" class="btn btn-primary mb-3">+ create new</a>
					@endif
					@if ($list->links())
						<div class="float-right">
							{{ $list->links() }}
						</div>
					@endif
					<div class="table-responsive">
						<table id="donors" class="table table-bordered table-hover text-nowrap" style="font-size:12px">
							<thead>
							<tr>
								<th style="width: 30px; text-align: center">#</th>
								<th class="text-primary">Student</th>
								<th class="text-primary">Grade</th>
								<th class="text-primary">Exam</th>
								<th class="text-primary">Score value</th>
								<th class="text-primary">Date</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							@if (count($list))
								@foreach($list as $rec)
									<tr>
										<td class="bg-lightblue text-center">
											{{ ($list->firstItem() + $loop->index) }}
										</td>
										<td style="white-space: break-spaces;width:200px;">{{ $rec['student']['full_name'] }}</td>
										<td style="white-space: break-spaces;width:50px;">{{ $rec['grade']['name'] }}</td>
										<td style="white-space: break-spaces;width:200px;">{{ $rec['exam']['title'] }}</td>
										<td style="width:100px;color:#fff;background:{{$rec->color}};white-space: break-spaces;">{{ $rec['score'] }}</td>
										<td style="white-space: normal;width:50px;">{{ date('d.m.Y', strtotime($rec['date'])) }}</td>
										<td class="text-center" style="width:50px;">
											<a href="{{ route('scores.show', ['score' => $rec->id]) }}" class="btn btn-primary btn-sm float-left mr-1 hidden">
												<i class="fas fa-eye"></i> 
											</a>
											@if(current_user()->is_admin())
												<a href="{{ route('scores.edit', ['score' => $rec->id]) }}" class="btn btn-info btn-sm float-left mr-1">
													<i class="fas fa-pencil-alt"></i> </a>
												<form action="{{ route('scores.destroy', ['score' => $rec->id]) }}" method="post">
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Confirm delete score?')">
														<i class="fas fa-trash-alt"></i>
													</button>
												</form>
											@endif
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="4">
										<p class="text-gray text-lg">nothing found...</p>
									</td>
								</tr>
							@endif

							</tbody>
						</table>
					</div>
					@if ($list->links())
						<h4 class="float-left">Showing {{$list->firstItem()}} to {{$list->lastItem()}} of {{$list->total()}} entries </h4>
						<div class="float-right">
							{{ $list->links() }}
						</div>
					@endif
				</div>
            </div>
        </div>
    </div>
@stop
