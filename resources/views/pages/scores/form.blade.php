
<form role="form" method="post" action="{{ isset($rec) ? route('scores.update', ['score' => $rec->id]) : route('scores.store')}}">
	@if($readonly ?? false)
	@else
		@csrf
		@if(isset($rec))
			@method('PUT')
		@else
			@method('POST')
		@endif
	@endif
	<div class="card-body">
		 <div class="row">
			<div class="col-sm-3">
				<div class="form-group {{ ($rec ?? false) ? 'disabled' : '' }}">
					<label>Student</label>
					<select name="student_id" class="select2" required data-placeholder="- select student -">
						<option></option>
						@foreach($students as $student)
							<option value="{{ $student->id }}"
								data-level="{{ $student->grade->level }}"
								data-adv="{{$student->grade->name}}"
								{{ old('student_id', $rec['student_id'] ?? '') === $student->id ? ' selected' : ''}}
							>
								{{ $student->full_name }}
							</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group {{ ($rec ?? false) ? 'disabled':''}}">
					<label>
						Select
						<span class="text-green">exam</span>
					</label>
					<select name="exam_id" class="custom-select" required>
						<option>- select exam -</option>
						@foreach($exams as $exam)
							<option value="{{ $exam->id }}" data-level="{{ $exam->grade_level }}"
								{{ old('exam_id', $rec['exam_id'] ?? '') === $exam->id ? ' selected' : ''}}
							>
								{{ $exam->title }}
							</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label>
						Date
					</label>
					<div class="input-group date" id="scoredate" 
						data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input @error('score') is-invalid @enderror" 
							name="date" {{($readonly ?? false) ? 'readonly' : ''}} 
							value="{{ old('date', $rec['date'] ?? '') }}"
							data-target="#scoredate"/>
                        <div class="input-group-append" data-target="#scoredate" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
					@error('date')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label>
						Score
					</label>
					<input type="number" max="100" min="0" name="score" {{($readonly ?? false) ? 'readonly' : ''}}
					class="form-control @error('score') is-invalid @enderror"
					value="{{ old('score', $rec['score'] ?? '') }}">
					@error('score')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
			</div>
		</div>
	</div>
	
    <div class="card-footer">
        @if(!$readonly)
            <button type="submit" class="btn btn-primary col-sm-3 float-right ml-2">Save</button>
        @endif
        <a href="{{ route('scores.index') }}" class="btn btn-default col-sm-2 float-right">Back</a>
    </div>
</form>

@push('css')
	<link rel="stylesheet" href="/vendor/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
@endpush
@push('js')
	<script src="/vendor/moment/moment.min.js"></script>
	<script src="/vendor/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>

	<script>
		$(function(){
			$('#scoredate').datetimepicker({
				format: 'YYYY-MM-DD'
			});

		});
	</script>
@endpush