@extends('adminlte::page')

@section('title', ' | Edit score')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit score</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('scores.index') }}">
                                Scores
                            </a>
                        </li>
                        <li class="breadcrumb-item active">Edit score</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-success">
                        <!-- /.card-header -->
                        @include('pages.scores.form', ['students'=>$students, 'exams'=>$exams, 'rec'=>$rec, 'readonly'=>false])
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop


@section('plugins.Select2', true)

@push('js')
    <script>
		$(function () {
            /*ChartJS*/
			var tick = {
				fontColor: '#495057',
				fontStyle: 'bold'
			};
			$('.select2').select2({
				theme: "bootstrap4",
				width: "style",
				placeholder: '- any -',
				allowClear: true,
				escapeMarkup: function (markup) {
					return markup;
				},
				templateResult: function (data) {
					var _custom = $(data.element).data('adv');
					if (!!_custom) {
						return $('<span>' + data.text + '</span><b style="float:right">' + _custom + '</b>');
					}
					return data.text;
				},
				templateSelection: function (data) {
					var _custom = $(data.element).data('adv');
					if (!!_custom) {
						return $('<span>' + data.text + '</span><b style="float:right">' + _custom + '</b>');
					}
					return data.text;
				}
			});
			bsCustomFileInput.init();
		});
	</script>
@endpush