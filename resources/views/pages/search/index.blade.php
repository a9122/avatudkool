@extends('adminlte::page')

@section('title', ' | Search result')

@section('content')<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <form action="{{ route('search') }}">
            @csrf
            @method('GET')
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <input type="search" class="form-control form-control-lg"
                                name="q"
                                placeholder="Type your keywords here.." value="{{ $q }}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="list-group">
                    @each('')
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@stop
@section('css')
@stop
@push('js')
    <script>
		$(function () {

		});
    </script>
@endpush