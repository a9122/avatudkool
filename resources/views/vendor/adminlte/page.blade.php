@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/lobibox/css/lobibox.min.css') }}">
    <link rel="stylesheet" href="{{ asset_ext('css/main.css') }}">

    @stack('css')
    @yield('css')

    <script src="{{ asset('vendor/nprogress/nprogress.js') }}"></script>
@stop

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')
    <div class="wrapper">

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        @empty($iFrameEnabled)
            @include('adminlte::partials.cwrapper.cwrapper-default')
        @else
            @include('adminlte::partials.cwrapper.cwrapper-iframe')
        @endempty

        {{-- Footer --}}
        @hasSection('footer')
            @include('adminlte::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif

    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/lobibox/js/lobibox.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.ajaxSubmit.js') }}"></script>
    <script src="{{ asset_ext('js/main.js') }}"></script>
    <script>
		$(window).on('load', function () {
			NProgress.done();
		})
		$(function () {
			$('[data-toggle="tooltip"]').tooltip();

            @if(session('toast.success'))
			UserNotice.success('{{session('toast.success')}}', 'Success');
            @endif

            @if(session('toast.error'))
			UserNotice.error('{{session('toast.error')}}', 'Error');
            @endif

            @if(session('toast.info'))
			UserNotice.info('{{session('toast.info')}}', 'Notification');
            @endif
		});
    </script>

    @stack('js')
    @yield('js')
@stop
