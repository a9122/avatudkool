<li @isset($item['id']) id="{{ $item['id'] }}" @endisset class="nav-item">

    <a class="nav-link {{ $item['class'] }}" href="{{ $item['href'] }}"
       @isset($item['target']) target="{{ $item['target'] }}" @endisset
       {!! $item['data-compiled'] ?? '' !!}>

        {{-- Icon (optional) --}}
        @isset($item['icon'])
            <i class="{{ $item['icon'] }} {{
                isset($item['icon_color']) ? 'text-' . $item['icon_color'] : ''
            }}"></i>
        @endisset

        {{-- Text --}}
        {{ $item['text'] }}

        {{-- Label (optional) --}}
        @php
            $count = 0;
            if(isset($item['label'])){
                $count = !is_numeric($item['label']) && is_string($item['label'])
                    ? records_count($item['label'])
                    : $item['label'];
            }
        @endphp
        @if($count != 0 || ($item['label_show_empty'] ?? true))
            @isset($item['label'])
                <span class="badge badge-{{ $item['label_color'] ?? 'primary' }} navbar-badge">
                    {{ $count }}
                </span>
            @endisset
        @endif

    </a>

</li>
