@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

{{-- Default Content Wrapper --}}
<div class="content-wrapper {{ config('adminlte.classes_content_wrapper', '') }}">

    {{-- Content Header --}}
    @hasSection('content_header')
        <div class="content-header">
            <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                @yield('content_header')
            </div>
        </div>
    @endif

    {{-- Main Content --}}
    <div class="content pt-10">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade-out">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <ul class="list-unstyled mb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('error')&& !empty(session()->get('error')))
                        <div class="alert alert-danger alert-dismissible fade-out">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ is_scalar(session('error')) ? session('error') : implode(' ', session('error')) }}
                        </div>
                    @endif
                    @if (session()->has('success') && !empty(session()->get('success')))
                        <div class="alert alert-success alert-dismissible fade-out">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ is_scalar(session('error')) ? session('error') : implode(' ', session('error')) }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
            @yield('content')
        </div>
    </div>
</div>