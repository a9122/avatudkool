@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

{{-- Default Content Wrapper --}}
<div class="content-wrapper {{ config('adminlte.classes_content_wrapper', '') }}">

    {{-- Content Header --}}
    @hasSection('content_header')
        <div class="content-header">
            <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                @yield('content_header')
            </div>
        </div>
    @endif

    {{-- Main Content --}}
    <div class="content pt-10">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade-out">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <ul class="list-unstyled mb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('error'))
                        <div class="alert alert-danger alert-dismissible fade-out">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade-out">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
            @yield('content')
        </div>
    </div>
</div>
<div class="toast-container">

    {{-- error toast --}}
    @if (session()->has('error.toast'))
        <div class="toast flash-toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" role="img" aria-label=" :  " preserveAspectRatio="xMidYMid slice" focusable="false">
                    <title></title>
                    <rect width="100%" height="100%" fill="#dc3545"></rect>
                    <text x="50%" y="50%" fill="#dee2e6" dy=".3em"></text>
                </svg>
                <strong class="mr-auto text-error">Error</strong>
                <small class="text-muted">{{env('APP_NAME')}}</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                {{ session('error-toast') }}
            </div>
        </div>
    @endif
    {{-- info toast --}}
    @if (session()->has('info-toast'))
        <div class="toast flash-toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" role="img" aria-label=" :  " preserveAspectRatio="xMidYMid slice" focusable="false">
                    <title></title>
                    <rect width="100%" height="100%" fill="#007aff"></rect>
                    <text x="50%" y="50%" fill="#dee2e6" dy=".3em"></text>
                </svg>
                <strong class="mr-auto text-info">Info</strong>
                <small class="text-muted">{{env('APP_NAME')}}</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                {{ session('info-toast') }}
            </div>
        </div>
    @endif
    {{-- success toast --}}
    @if (session()->has('success-toast'))
        <div class="toast flash-toast" role="alert"
            aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" role="img" aria-label=" :  " preserveAspectRatio="xMidYMid slice" focusable="false">
                    <title></title>
                    <rect width="100%" height="100%" fill="#28A745"></rect>
                    <text x="50%" y="50%" fill="#dee2e6" dy=".3em"></text>
                </svg>
                <strong class="mr-auto text-success">Success</strong>
                <small class="text-muted"> {{env('APP_NAME')}} </small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                {{ session('success-toast') }}
            </div>
        </div>
    @endif
</div>
@push('js')
    <script>
		$(function () {
			$('.toast.flash-toast').toast({
				delay: 4000,
				autohide: false
			});

			setTimeout(function () {
				$('.toast.flash-toast').each(function () {
					var _toast = $(this);

					setTimeout(function () {
						_toast.toast('show');
					}, 100);
				});
			}, 200);
		});
    </script>
@endpush