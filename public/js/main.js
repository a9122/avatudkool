var UserNotice = {
	_defaultOptions: {
		icon: true,
		closable: true,
		closeOnClick: true,
		iconSource: "fontAwesome",
		width: 400,
		sound: false,
		delayIndicator: true,
		position: 'top right',
		showClass: 'fadeInDown animated-super-fast',
		hideClass: 'zoomOut',
		messageHeight: 64,
		delay: 5000,
	},
	_notify: function (type, options) {
		var _default_options = this._defaultOptions;
		return Lobibox.notify(type, $.extend({}, _default_options, options || {}));
	},

	info: function (text, title, options) {
		var _options = options || {};

		return this._notify('info', $.extend({}, _options, {
			'msg': text,
			'title': title || window.location.host
		}));
	},
	success: function (text, title, options) {
		var _options = options || {};

		return this._notify('success', $.extend({}, _options, {
			'msg': text,
			'title': title || window.location.host
		}));
	},
	error: function (text, title, options) {
		var _options = options || {};

		return this._notify('error', $.extend({}, _options, {
			'msg': text,
			'title': title || window.location.host
		}));
	},
	warning: function (text, title, options) {
		var _options = options || {};

		return this._notify('warning', $.extend({}, _options, {
			'msg': text,
			'title': title || window.location.host
		}));
	},
	/* confirm */
	confirm: function (msg, title, on_confirm, on_cancel, post) {
		var _opts = {
			text: "Are you sure?",
			title: "",
			confirmButton: "Yes",
			cancelButton: "Cancel",
			post: false,
			submitForm: false,
			confirmButtonClass: "btn-primary",
			cancelButtonClass: "btn-default",
			dialogClass: "modal-dialog",
			modalOptionsBackdrop: true,
			modalOptionsKeyboard: true
		};

		Lobibox.confirm({
			msg: msg || "Are you sure?",
			title: title || '',
			callback: function ($this, choise) {
				if (choise === 'yes') {

					if ((on_confirm instanceof jQuery) && (on_confirm.is('form'))) {
						_opts.submitForm = on_confirm;
					}
					if ((on_confirm instanceof jQuery) && (on_confirm.is('form'))) {
						on_confirm.trigger('submit');
					} else if (isFunction(on_confirm)) {
						on_confirm();
					}
				} else if (choise === 'no') {
					if (isFunction(on_cancel)) {
						on_cancel();
					}
				}
			}
		});
	}
};

$(function (){
	if (NProgress){
		NProgress.configure({trickle: true, showSpinner: false});
		setTimeout(function () {
			NProgress.done(1);
		}, 200);
	}
	$('input[name="user_type"][value="admin"]').on('change', function(e){
		if ($(this).is(':checked')){
			$(this).parents('form').find('.hide-if-admin').hide();
			$(this).parents('form').find('.hide-if-admin input').prop('disabled', true);
		} else {
			$(this).parents('form').find('.hide-if-admin').show();
			$(this).parents('form').find('.hide-if-admin input').prop('disabled', false);
		}
	});
	$('body').on('click', '.password-show-toggle', function (e) {
		e.stopPropagation();
		e.preventDefault();
		var _target = $(this).parents('.input-group').find('input[data-toggle="password"]');

		console.log($(this), _target);
		if (_target.length){
			if (_target.attr('type') === 'password'){
				_target.attr('type', 'text');
				$(this).find('.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash');
			} else {
				_target.attr('type', 'password');
				$(this).find('.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye');
			}
		}
	}).on('click', 'tr[data-toggle-row]', function (e) {
		e.stopPropagation();
		if ($(this).hasClass('loaded')){
			$(this).find('button[data-toggle-row]').trigger('click');
		} else {
			var _tr = $(this);
			var _req_link = $(this).data('link');
			NProgress.start();
			_tr.addClass('disabled');
			_tr.find('.loader').show();
			$.get(_req_link, function (data) {
				$(data).insertAfter(_tr);
				_tr.find('.loader').hide();
				NProgress.done();
				_tr.removeClass('disabled').addClass('loaded');
				setTimeout(function(){
					_tr.find('button[data-toggle-row]').trigger('click');
				},50)
			});
		}
	}).on('click', 'button[data-toggle-row]', function (e) {
		e.stopPropagation();
		e.preventDefault();

		var _tr = $(this).parents('tr.anchor');
		if (!_tr.hasClass('loaded')) {
			var _req_link = _tr.data('link');

			NProgress.start();
			_tr.addClass('disabled');
			_tr.find('.loader').show();
			$.get(_req_link, function (data) {
				$(data).insertAfter(_tr);
				_tr.find('.loader').hide();
				NProgress.done();
				_tr.removeClass('disabled').addClass('loaded');
				setTimeout(function () {
					_tr.find('button[data-toggle-row]').trigger('click');
				}, 50)
			});

			return;
		}

		var _r      = $(this).data('toggle-row');
		var _target = $(this).parents('tbody').find('tr[data-row="' + _r + '"]');

		if (_target.hasClass('hidden')) {
			$(this).find('.fas.fa-chevron-down')
				.removeClass('fa-chevron-down')
				.addClass('fa-chevron-up');
		} else {
			$(this).find('.fas.fa-chevron-up')
				.removeClass('fa-chevron-up')
				.addClass('fa-chevron-down');
		}
		_target.toggleClass('hidden');
	});

	$('form input.search-title').on('search', function(e){
		if (!$(this).val().length){
			location.href = location.pathname;
		}
	});

	$('.select2').select2({
		theme: "bootstrap4",
		width: "style"
	});
	bsCustomFileInput.init();
})