<?php

use Database\Seeders\DomainSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainTables extends Migration {
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('subjects');
		Schema::dropIfExists('grades');
		Schema::dropIfExists('exams');
		Schema::dropIfExists('students');
		Schema::dropIfExists('scores');
	}

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('subjects', function(Blueprint $table) {
			$table->charset   = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

			$table->id();
			$table->string('name')->unique();
			$table->text('description')->nullable();

			$table->foreignId('user_id')->nullable()
				->constrained('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->timestamps();
		});

		Schema::create('grades', function(Blueprint $table) {
			$table->charset   = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

			$table->id();
			$table->unsignedTinyInteger('level');
			$table->string('group', 20)->nullable();
			$table->text('description')->nullable();

			$table->foreignId('user_id')->nullable()
				->constrained('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->timestamps();
		});

		/*$table->year('birth_year');*/
		Schema::create('students', function(Blueprint $table) {
			$table->charset   = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

			$table->id();
			$table->string('first_name')->index();
			$table->string('last_name')->index();
			$table->index(['first_name', 'last_name']);
			
			$table->string('isikukood', 10)->unique()->nullable();
			$table->enum('lang', ['est', 'rus'])->default('est');
			$table->unsignedTinyInteger('status')->default(1)->comment('0 - not active; 1 - active; 2 - alumnus;');
			
			$table->foreignId('grade_id')
				->constrained('grades')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreignId('user_id')->nullable()
				->constrained('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->text('description')->nullable();
			$table->json('_data')->nullable();
			$table->timestamps();
		});

		Schema::create('exams', function(Blueprint $table) {
			$table->charset   = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

			$table->id();
			$table->string('name')->nullable();
			$table->foreignId('subject_id')
				->constrained('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->unsignedTinyInteger('grade_level')->nullable();
			$table->enum('type', ['school', 'government', 'studium'])->default('school')->index();
			$table->enum('semester', ['autumn', 'spring'])->default('autumn');
			$table->foreignId('user_id')->nullable()
				->constrained('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			
			$table->text('description')->nullable();
			$table->json('_data')->nullable();

			$table->timestamps();
		});

		Schema::create('scores', function(Blueprint $table) {
			$table->charset   = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

			$table->id();
			$table->foreignId('exam_id')
				->constrained('exams')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreignId('student_id')
				->constrained('students')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreignId('grade_id')
				->constrained('grades')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->date('date');
			$table->unsignedTinyInteger('score')->default(0);

			$table->foreignId('user_id')->nullable()
				->constrained('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			
			$table->json('_data')->nullable();
			$table->timestamps();
		});
	}
}
