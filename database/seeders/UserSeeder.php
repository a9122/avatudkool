<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use function bcrypt;
use function now;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->insert([
		    'name'              => 'Admin',
		    'email'             => 'it@avatudkool.ee',
		    'email_verified_at' => now(),
		    'type'              => 'admin',
		    'password'          => bcrypt('admin123'),
		    'created_at'        => now()
	    ]);
    }
}
