<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class GradeSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$dataset = [
			'2',
			'3',
			'4',
			'5',
			'6',
		];
		foreach($dataset as $level) {
			foreach(['Alpha', 'Bravo', 'Venus'] as $grp) {
				DB::table('grades')->insert([
					'level'   => $level,
					'group'   => $grp,
					'user_id' => 1,
					'created_at' => now()
				]);
			}
		}
	}
}


