<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use function bcrypt;
use function now;

class TeacherSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		for($i = 0; $i < 6; $i++) {
			$n = $i + 1;
			DB::table('users')->insert([
				'name'              => 'Teacher '.$n,
				'email'             => 'teacher'.$n.'@mail.com',
				'email_verified_at' => now(),
				'type'              => 'teacher',
				'password'          => bcrypt('teacher'.$n),
			    'created_at'        => now()
			]);
		}
	}
}
