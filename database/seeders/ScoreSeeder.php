<?php
namespace Database\Seeders;

use App\Models\Grade;
use App\Models\Exam;
use App\Models\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScoreSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$students = Student::all();
		
		foreach($students as $student){
			$exams = Exam::getAllByGrade($student->grade_id);
			foreach($exams as $exam){
				DB::table('scores')->insert([
					'student_id' => $student->id,
					'grade_id'   => $student->grade_id,
					'exam_id'    => $exam->id,
					'date'	     => ($exam->semester == 'autumn') ? '2021-12-01' : '2022-04-01',
					'score'	     => rand(45, 90),
					'created_at' => now(),
					'user_id'	 => 1				
				]);
			}
			
			if ($student->grade->level > 3){
				/* curr - 1 */
				$prev_grade = Grade::getPrevGrade($student->grade_id);
				if ($prev_grade){
					$exams = Exam::getAllByGrade($prev_grade->id);
					foreach($exams as $exam){
						DB::table('scores')->insert([
							'student_id' => $student->id,
							'grade_id'   => $prev_grade->id,
							'exam_id'    => $exam->id,
							'date'	     => ($exam->semester == 'autumn') ? '2020-12-02' : '2021-05-01',
							'score'	     => rand(35, 95),
							'created_at' => now(),
							'user_id'	 => 1
						]);
					}
				}
				
				/* curr - 2 */
				$prev_grade = Grade::getPrevGrade($prev_grade->id);
				if ($prev_grade){
					$exams = Exam::getAllByGrade($prev_grade->id);
					foreach($exams as $exam){
						DB::table('scores')->insert([
							'student_id' => $student->id,
							'grade_id'   => $prev_grade->id,
							'exam_id'    => $exam->id,
							'date'	     => ($exam->semester == 'autumn') ? '2019-12-03' : '2020-04-29',
							'score'	     => rand(40, 97),
							'created_at' => now(),
							'user_id'	 => 1				
						]);
					}
				}
			}
		}
	}
}


