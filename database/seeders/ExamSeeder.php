<?php
namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExamSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$dataset = [
			['3', 'English', 'school', 'autumn'],
			['3', 'English', 'school', 'spring'],
			['4', 'English', 'school', 'spring'],
			['5', 'English', 'school', 'spring'],
			['6', 'English', 'school', 'spring'],
			['4', 'Estonian for russian speaking', 'government', 'autumn'],
			['2', 'Math', 'school', 'spring'],
			['3', 'Math', 'school', 'spring'],
			['4', 'Math', 'school', 'spring'],
			['4', 'Math', 'government', 'autumn'],
			['5', 'Math', 'school', 'spring'],
			['6', 'Math', 'school', 'spring'],
			['4', 'Mother tongue', 'government', 'autumn'],
			['4', 'Science', 'government', 'autumn'],
			['2', 'Target language', 'school', 'autumn'],
			['2', 'Target language', 'school', 'spring'],
			['3', 'Target language', 'school', 'spring'],
			['4', 'Target language', 'school', 'spring'],
			['5', 'Target language', 'school', 'spring'],
			['6', 'Target language', 'school', 'spring'],
		];
		foreach($dataset as $data) {
			DB::table('exams')->insert([
				'name'        => trim(implode(', ', [$data[1], $data[0], $data[2], $data[3]])),
				'subject_id'  => Subject::where('name', 'LIKE', $data[1])->first()->id,
				'grade_level' => (int)$data[0],
				'type'        => $data[2],
				'semester'    => $data[3],
				'user_id'     => 1,
				'created_at'  => now()
			]);
		}
	}
}


