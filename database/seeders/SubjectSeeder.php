<?php
/*
 * Project: linksinspector.local;
 * Creator: olegt;
 */


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$dataset = [
			'English',
			'Estonian for russian speaking',
			'Math', 
			'Mother tongue',
			'Science',
			'Target language', 
		];
		foreach($dataset as $subj) {
			DB::table('subjects')->insert([
				'name' => $subj,
				'user_id' => 1,
				'created_at' => now()
			]);
		}
	}
}


