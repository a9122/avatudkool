<?php
namespace Database\Seeders;


use App\Models\Grade;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use function rand;


class StudentSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Factory::create('en_US');

		for($i = 0; $i < 63; $i++):
			$grade_lvl = ($i % 5) + 2;
			$grade_id  = Grade::where('level', $grade_lvl)->where('group', $faker->randomElement(['Alpha', 'Bravo', 'Venus']))->first('id')->id;
			DB::table('students')
				->insert([
					'first_name' => $faker->firstName(),
					'last_name'  => $faker->lastName(),
					'isikukood'  => rand(5, 6).'0'.rand(4, 9).$faker->numerify('########'),
					'grade_id'   => $grade_id,
					'lang'       => $faker->randomElement(['est', 'rus']),
					'status'  	 => 1,
					'user_id'    => 1,
					'created_at' => now(),
				]);
		endfor;
	}
}


