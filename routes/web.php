<?php

use Illuminate\Support\Facades\Route;

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
	Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::get('/student-scores', [App\Http\Controllers\HomeController::class, 'studentScores'])
		->name('home.student-scores');
	Route::get('/grade-scores', [App\Http\Controllers\HomeController::class, 'gradeScores'])
		->name('home.grade-scores');
	Route::any('search', [App\Http\Controllers\HomeController::class, 'search'])
		->name('home.search');

	// Route::post('/filter-grade', [App\Http\Controllers\HomeController::class, ''])->name('');
	// Route::post('/filter-subject', [App\Http\Controllers\HomeController::class, ''])->name('');
	// Route::post('/filter-student', [App\Http\Controllers\HomeController::class, ''])->name('');

	Route::resources([
		'accounts' => App\Http\Controllers\AccountController::class,
		'exams'    => App\Http\Controllers\ExamController::class,
		'grades'   => App\Http\Controllers\GradeController::class,
		'scores'   => App\Http\Controllers\ScoreController::class,
		'students' => App\Http\Controllers\StudentController::class,
		'subjects' => App\Http\Controllers\SubjectController::class,
	]);

	Route::get('/teachers', [App\Http\Controllers\AccountController::class, 'indexTeachers'])
		->name('school.teachers');

	Route::post('/classroom/{grade}/promote/{to_grade?}',
		[App\Http\Controllers\ClassroomController::class, 'promote'])->name('school.classroom.promote');
	Route::post('/classroom/{grade}/graduate/',
		[App\Http\Controllers\ClassroomController::class, 'graduate'])->name('school.classroom.graduate');
	Route::get('/classroom/{grade}',
		[App\Http\Controllers\ClassroomController::class, 'scores'])->name('school.classroom.scores');

	Route::get('/import', [App\Http\Controllers\ImportController::class, 'index'])
		->name('import.index');
	Route::post('/import', [App\Http\Controllers\ImportController::class, 'import'])
		->name('import.process');


	/* for filtering scores */
	Route::prefix('search')->group(function() {
		/* by grade and years */
		/* by student and years */
		/* by subject and sudents */
	});
});